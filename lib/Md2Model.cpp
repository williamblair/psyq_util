#include <Md2Model.h>

Md2Model::Md2Anim Md2Model::stdMd2Anims[MD2ANIM_COUNT] =
{
    {   0,    39,   9 }, // Stand
    {  40,    45,  10 }, // Run
    {  46,    53,  10 }, // Attack
    {  54,    57,   7 }, // Pain A
    {  58,    61,   7 }, // Pain B
    {  62,    65,   7 }, // Pain C
    {  66,    71,   7 }, // Jump
    {  72,    83,   7 }, // Flip
    {  84,    94,   7 }, // Salute
    {  95,   111,  10 }, // Fall back
    { 112,   122,   7 }, // Wave
    { 123,   134,   6 }, // Point
    { 135,   153,  10 }, // Crouch stand
    { 154,   159,   7 }, // Crouch walk
    { 160,   168,  10 }, // Crouch attack
    { 196,   172,   7 }, // Crouch pain
    { 173,   177,   5 }, // Crouch death
    { 178,   183,   7 }, // Death fall back
    { 184,   189,   7 }, // Death fall forward
    { 190,   197,   7 }, // Death fall back slow
    { 198,   198,   5 }  // Boom
};

Md2Model::Md2Model() :
    md2Header(NULL),
    md2Skins(NULL),
    md2TexCoords(NULL),
    md2Triangles(NULL),
    md2Keyframes(NULL),
    md2TriangleCount(0),
    md2VertesPerFrame(0),
    md2FrameCount(0),
    md2Scale(1.0f)
{
    animState.Clear();
    u_int start, end, frameRate;
    GetStdAnim( MD2ANIM_STAND, start, end, frameRate );
    animState.startFrame = start;
    animState.endFrame = end;
    animState.nextFrame = start + 1;
    animState.fps = frameRate;
    animState.interp = 0.0f;
}

bool Md2Model::InitFromMemory(const u_char* data, u_int sizeBytes, float scale)
{
    if (data == NULL || sizeBytes == 0)
    {
        printf("Invalid data buffer\n");
        return false;
    }
    
    printf("Importing MD2 from data buffer\n");
    
    // read Header
    md2Header = (Header*)data;
    if (md2Header->magic != chars2uint('I','D','P','2'))
    {
        printf("MD2 Header magic invalid, continuing\n");
    }
    if (md2Header->version != MD2_GOOD_VERSION)
    {
        printf("Bad MD2 version %u, want %u\n", md2Header->version, MD2_GOOD_VERSION);
    }
    
    if (md2Header->frameCount == 0)
    {
        printf("MD2 Frame count 0!\n");
        return false;
    }
    if (md2Header->offsetEnd > sizeBytes)
    {
        printf("Md2 buffer too small\n");
        return false;
    }
    
    if (md2Header->texCoordCount == 0)
    {
        printf("Md2 bad tex coord count\n");
        return false;
    }
    if (md2Header->triangleCount == 0)
    {
        printf("Md2 bad triangle count\n");
        return false;
    }

    if ((md2Header->offsetSkins     + md2Header->skinCount)     * sizeof(Skin) >= sizeBytes     || 
        (md2Header->offsetTexCoords + md2Header->texCoordCount) * sizeof(TexCoord) >= sizeBytes ||
        (md2Header->offsetTris      + md2Header->triangleCount) * sizeof(Triangle) >= sizeBytes)
    {
        printf("MD2 header bad offsets\n");
        return false;
    }

    md2Scale = scale;
    
    md2TriangleCount = md2Header->triangleCount;
    md2VertesPerFrame = md2Header->vertsPerFrame;
    md2FrameCount = md2Header->frameCount;
    
    if (md2Header->skinCount >= MD2_MAX_SKINS)
    {
        printf("Model data contains more skin than Quake II supports\n");
        return false;
    }
    if (md2Header->frameCount >= MD2_MAX_KEYFRAMES)
    {
        printf("Model data contains more keyframes than Quake II supports\n");
        return false;
    }
    if (md2Header->vertsPerFrame >= MD2_MAX_VERTS)
    {
        printf("Model data contains more vertices than Quake II supports\n");
        return false;
    }
    if (md2Header->triangleCount >= MD2_MAX_TRIS)
    {
        printf("Model data contains more triangles than Quake II supports\n");
        return false;
    }

    printf("MD2 importing skins, texCoords, tris\n");

    if (md2Header->skinCount != 0)
    {
        md2Skins = (Skin*)(data + md2Header->offsetSkins);
    }
    
    // read texture coordinates and triangles (mandatory)
    md2TexCoords = (const TexCoord*)(data + md2Header->offsetTexCoords);
    md2Triangles = (const Triangle*)(data + md2Header->offsetTris);
    
    printf("Importing MD2 keyframes\n");

    // note - each keyframe is followed by a vertex packet
    md2Keyframes = (const Keyframe*)(data + md2Header->offsetFrames);
    
    // pre-allocate a bounding box for each frame
    // TODO - computing the AABB for frames causes error somewhere
    //md2FrameBounds.resize(md2FrameCount);
    for (u_int f = 0; f < md2FrameCount; f++)
    {
        if ((f * md2VertesPerFrame) >= (md2FrameCount * md2VertesPerFrame))
        {
            printf("Bad vertex offset in Md2 frame %u\n", f);
            return false;
        }
        //ComputeAabbForFrame(f);
    }
    
    SetupAnimations();
    printf("MD2 import completed\n");
    return true;
}

void Md2Model::SetAnim( StdMd2AnimId animId )
{
    animState.Clear();
    u_int start, end, frameRate;
    GetStdAnim( animId, start, end, frameRate );
    animState.startFrame = start;
    animState.endFrame = end;
    animState.nextFrame = start + 1;
    animState.fps = frameRate;
    animState.interp = 0.0f;
}

void Md2Model::AssembleFrame(u_int frameIndex, 
                    u_int skinWidth, u_int skinHeight, 
                    const Color4f& vertColor/*, 
                    DrawVertex* drawVerts*/) const
{
    //assert(md2Header != NULL)
    //assert(md2TexCoords != NULL)
    //assert(md2Triangles != NULL)
    //assert(md2Keyframes != NULL)
    //assert(drawVerts != NULL)

    #define color2char(c) ((u_char)(255.0f*(c)))
    
    const Keyframe& frame = GetKeyframe(frameIndex);
    const PSXVector vColor(color2char(vertColor.r), color2char(vertColor.g), color2char(vertColor.b), /*vertColor.a*/255);
    
    //printf("MD2 scale: %f\n", md2Scale);
    
    // for each triangle
    for (u_int t = 0; t < md2TriangleCount; t++)
    {
        // for each triangle vertex
        for (u_int v = 0; v < 3; v++)
        {
            // fetch correct vertex and tex coords
            const Vertex& vert = GetFrameVertex(frameIndex, md2Triangles[t].vertex[v]);
            const TexCoord& texc = md2TexCoords[md2Triangles[t].uv[v]];
            
            // compute final tex coords
            const float uvX = ((float)texc.u) / skinWidth;
            const float uvY = ((float)texc.v) / skinHeight;
            
            // uncompress the vertex position and transform
            // swap y-z because Quake's coord system is different from standard setup
            PSXVector xyzPos;
            xyzPos.x = float2fixed(((frame.scale[0] * vert.v[0]) + frame.translate[0]) * md2Scale);
            xyzPos.z = float2fixed(((frame.scale[1] * vert.v[1]) + frame.translate[1]) * md2Scale);
            xyzPos.y = float2fixed(((frame.scale[2] * vert.v[2]) + frame.translate[2]) * md2Scale);
            
            // TODO
            // store the new drawVertex
            //packDrawVertex(drawVerts[v], xyzPos, PSXVector(uvX, uvY, 0, ONE), vColor);
        }
        
        // TODO
        //drawVerts += 3;
    }
}

void Md2Model::AssembleFrameInterpolated(u_int frameA, u_int frameB, 
                                float interp, 
                                u_int skinWidth, u_int skinHeight, 
                                const Color4f& vertColor/*, 
                                DrawVertex* drawVerts*/) const
{
    // assert(md2Header != NULL)
    // assert(md2TexCoords != NULL)
    // assert(md2Triangles != NULL)
    // assert(md2Keyframes != NULL)
    // assert(drawVertex != NULL)
    
    const Keyframe& keyframeA = GetKeyframe(frameA);
    const Keyframe& keyframeB = GetKeyframe(frameB);
    const PSXVector vColor(vertColor.r, vertColor.g, vertColor.b, vertColor.a);
    
    // for each triangle
    for (u_int t = 0; t < md2TriangleCount; t++)
    {
        // for each triangle vertex
        for (u_int v = 0; v < 3; v++)
        {
            const Vertex& vertA = GetFrameVertex(frameA, md2Triangles[t].vertex[v]);
            const Vertex& vertB = GetFrameVertex(frameB, md2Triangles[t].vertex[v]);
            const TexCoord& texc = md2TexCoords[md2Triangles[t].uv[v]];
            
            // compute final tex coords
            const float uvX = ((float)texc.u) / skinWidth;
            const float uvY = ((float)texc.v) / skinHeight;
            
            // compute final vertex position
            // swap y and z because Quake's coordinate system is different
            PSXVector vecA;
            PSXVector vecB;
            vecA.x = float2fixed((keyframeA.scale[0] * vertA.v[0]) + keyframeA.translate[0]);
            vecA.z = float2fixed((keyframeA.scale[1] * vertA.v[1]) + keyframeA.translate[1]);
            vecA.y = float2fixed((keyframeA.scale[2] * vertA.v[2]) + keyframeA.translate[2]);
            vecB.x = float2fixed((keyframeB.scale[0] * vertB.v[0]) + keyframeB.translate[0]);
            vecB.z = float2fixed((keyframeB.scale[1] * vertB.v[1]) + keyframeB.translate[1]);
            vecB.y = float2fixed((keyframeB.scale[2] * vertB.v[2]) + keyframeB.translate[2]);
            
            // linear interpolate final position and scale
            PSXVector xyzPos;
            lerpScale(xyzPos, vecA, vecB, float2fixed(interp), float2fixed(md2Scale));
            
            // TODO
            // store the new drawVertex
            //packDrawVertex(drawVerts[v], xyzPos, PSXVector(uvX, uvY, 0, ONE), vColor);
        }
        
        // TODO
        //drawVerts += 3;
    }
}

bool Md2Model::FindAnimByName(const char* name, u_int& start, u_int& end, u_int& fps) const
{
    //assert(name != NULL)
    for (u_int i = 0; i < md2Anims.size(); i++)
    {
        if (strcmp(md2Anims[i].name, name) == 0)
        {
            start = md2Anims[i].start;
            end = md2Anims[i].end;
            fps = GetAnimFpsForRange(start, end);
            return true;
        }
    }
    
    // failed to find
    start = 0;
    end = 0;
    fps = 0;
    return false;
}

bool Md2Model::FindAnimByPartialName(const char* partialName, u_int& start, u_int& end, u_int& fps) const
{
    //assert(partialName != NULL);
    
    for (u_int i = 0; i < md2Anims.size(); i++)
    {
        if (strstr(md2Anims[i].name, partialName) != NULL)
        {
            start = md2Anims[i].start;
            end = md2Anims[i].end;
            fps = GetAnimFpsForRange(start, end);
            return true;
        }
    }
    
    // failed to find
    start = 0;
    end = 0;
    fps = 0;
    return false;
}

bool Md2Model::FindAnimByIndex(u_int index, u_int& start, u_int& end, u_int& fps) const
{
    if (index >= md2Anims.size())
    {
        start = 0;
        end = 0;
        fps = 0;
        return false;
    }
    
    start = md2Anims[index].start;
    end = md2Anims[index].end;
    fps = GetAnimFpsForRange(start, end);
    return true;
}
