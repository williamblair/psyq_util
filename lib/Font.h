/*
 * Font.h
 */

#ifndef FONT_H_INCLUDED
#define FONT_H_INCLUDED

#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>

#include <Texture.h>

/*
===============================================================================
Font

    Holds a texture containing font characters and can draw strings with
    said texture

    Assumes characters are layed out from a to z top left to bottom right
    (like a book)

    lowercase only at the moment
===============================================================================
*/

class Font
{
public:

    friend class Renderer;

    /*
    ===================
    Constructor/Deconstructor
    ===================
    */
    Font();
    ~Font();

    /*
    ===================
    Parameters sent to the texture to load,
    See texture class for details
    ===================
    */
    void LoadTexture(
        u_long* texdata,  // pointer to texture data
        u_long* clutdata, // pointer to CLUT data (NULL if none)
        TEXTURE_BPP bpp,  // 0 - 16bit, 1 - 8bit, 2 - 4 bit
        int x,        // where in VRAM to load the texture
        int y,
        int w,        // size of the texture
        int h,
        int clutX,    // where to upload the clut data (if given)
        int clutY
    );

    /*
    ===================
    Set the size a single character takes up
    ===================
    */
    void SetWidthAndHeight( float width, float height );

    /*
    ===================
    Draw a string of characters using the internal texture
    ===================
    */
    void DrawString(u_int x, u_int y, char* message);

    // calculate the letter location in the texture
    inline void CalcCharUV( char letter, u_int* u, u_int* v )
    {
        int index = (u_int)letter - 97; // lowercase ascii 'a' starts at 97

        // the x (row) and y (column) index before scaling by character size
        u_int y_val = index / charsPerRow;
        u_int x_val = index % charsPerRow;

        // scale the index by character size
        x_val = (u_int)(x_val * fontWidth);
        y_val = (u_int)(y_val * fontHeight);

        // store the result
        *u = x_val;
        *v = y_val;
    }

    // calculate number location in the texture
    inline void CalcNumberUV( int num, u_int* u, u_int* v)
    {
        // TODO - configure/not hardcode
        numberStartRow = 4;
        int startY = (int)(numberStartRow * fontHeight);
        int index = num;

        u_int y_val = index / charsPerRow;
        u_int x_val = index % charsPerRow;

        // scale the index by character size
        x_val = (u_int)(x_val * fontWidth);
        y_val = (u_int)(y_val * fontHeight);
        y_val += startY;

        *u = x_val;
        *v = y_val;
    }

private:

    Texture texture;

    float fontWidth;
    float fontHeight;

    // based on the total texture width and the width of a single character
    u_int charsPerRow;

    // assuming numbers start on their own standalone row, 
    // underneath the characters
    int numberStartRow;
};

#endif // FONT_H_INCLUDED

