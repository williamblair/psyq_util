/*
 * Light.h
 */

#ifndef LIGHT_H_INCLUDED
#define LIGHT_H_INCLUDED

#include <sys/types.h>	// This provides typedefs needed by libgte.h and libgpu.h
#include <stdio.h>	// Not necessary but include it anyway
#include <string.h>
#include <stdlib.h>
#include <libetc.h>	// Includes some functions that controls the display
#include <libgte.h>	// GTE header, not really used but libgpu.h depends on it
#include <libgpu.h>	// GPU library header

/*
===============================================================================
Light

    Holds light color, position, rotation
===============================================================================
*/
class Light
{
public:

    friend class Renderer;

    /*
    ===================
    Constructor/Deconstructor
    ===================
    */
    Light();
    ~Light();

    /*
    ===================
    Move the ligh position
    ===================
    */
    void Translate( int x, int y, int z );
    void TranslateTo( int x, int y, int z );

    /*
    ===================
    Getters
    ===================
    */
    SVECTOR& GetAngle();

    /*
    ===================
    Setters
    ===================
    */
    void Rotate(   SVECTOR* angle );
    void RotateTo( SVECTOR* angle );

    void Rotate(   int x, int y, int z );
    void RotateTo( int x, int y, int z );

    void SetColor( int r, int g, int b );

private:
    SVECTOR angle;
    SVECTOR pos;
    VECTOR color;

    static VECTOR  DefaultColor;
    static SVECTOR DefaultAngle;
    static SVECTOR DefaultPos;
};

#endif // LIGHT_H_INCLUDED

