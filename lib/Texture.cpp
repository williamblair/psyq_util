/*
 * Texture.cpp
 */

#include "Texture.h"

/*
===================
Constructor/Deconstructor
===================
*/
Texture::Texture()
{
    clut_id  = 0;
    tpage_id = 0;

    width  = 0;
    height = 0;

    was_loaded = false;
}

Texture::~Texture()
{
}

/*
===================
Load texture data into VRAM
===================
*/
void Texture::Load(
    u_long *texdata,  // pointer to texture data
    u_long *clutdata, // pointer to CLUT data (NULL if none)
    TEXTURE_BPP bpp,  // 0 - 16bit, 1 - 8bit, 2 - 4 bit
    int x,        // where in VRAM to load the texture
    int y,
    int w,        // size of the texture
    int h,
    int clutX,    // where to upload the clut data (if given)
    int clutY
)
{
    if ( clutdata != NULL ) {
        clut_id = LoadClut(
            clutdata,         // clut pointer
            clutX, clutY      // VRAM coordinates
        );
    }

    tpage_id = LoadTPage(
        texdata,
        bpp,    // bit depth - 0=4bit, 1=8bit, 2=16bit
        0,      // semitransparency rate (default 0)
        x,      // x - destination from buffer address x
        y,      // y - destination frame buffer address y
        w,      // w - width
        h       // h - height
    );

    width  = w;
    height = h;

    was_loaded = true;
}

/*
==================
Use data from an existing texture
==================
*/
void Texture::CopyFrom( Texture& texture )
{
    was_loaded = true;

    tpage_id = texture.tpage_id;
    clut_id = texture.clut_id;
    width = texture.width;
    height = texture.height;
}

/*
===================
Tell a primitive to use this texture
===================
*/
void Texture::ApplyToPrimitive( POLY_FT3 *prim, TEXCOORD *tex_coord )
{
    setUV3(
        prim,
        tex_coord->u0*width, // U0, V0
        tex_coord->v0*height,
        tex_coord->u1*width, // U1, V1
        tex_coord->v1*height,
        tex_coord->u2*width, // U2, V2
        tex_coord->v2*height
    );

    prim->tpage = tpage_id;
    prim->clut  = clut_id;
}


/*
===================
Getters
===================
*/
u_short Texture::GetTexturePageId()
{
    return tpage_id;
}
u_short Texture::GetClutId()
{
    return clut_id;
}
int Texture::GetWidth()
{
    return width;
}
int Texture::GetHeight()
{
    return height;
}

/*
===================
A checkered image to use as default
Loaded via LoadDefault()
===================
*/
Texture Texture::DefaultTexture;
void Texture::LoadDefault()
{
    RECT texRect;
    size_t row = 0;
    size_t col = 0;
    size_t flipCounter = 0;
    u_short tex[32*32];
    u_short red = 0x801F;
    u_short white = 0xFFFF;
    u_short cur_color = red;
    for ( row = 0; row < 32; row++ )
    {
        for ( col = 0; col < 32; col++ )
        {
            if ( (col % 10) == 0 )
            {
                cur_color = (cur_color == red) ? white : red;
            }

            tex[row*32+col] = cur_color;
        }

        if ( row % 10 == 0 )
        {
            cur_color = (cur_color == red) ? white : red;
        }
    }

    DefaultTexture.tpage_id = LoadTPage( (u_long*)tex, 2, 0, 320, 0, 32, 32 );
}
