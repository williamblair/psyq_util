/*
 * Camera.h
 */

#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
===============================================================================

Camera

    First person camera which is used for the view matrix
===============================================================================
*/
class Camera
{
public:

    friend class Renderer;

    Camera()
    {
        FOV = 45.0f;
        yaw = -90.0f;
        pitch = 0.0f;

        up.vx = 0;
        up.vy = ONE;
        up.vz = 0;

        setVector( &front, 0, 0, -ONE );
        setVector( &position, 0, 0, 3*ONE );

        cross_product( &front, &up, &right );
        VectorNormal( &right, &right );

        updateFront();
        UpdateMat();
    }

    void Rotate( const float x, const float y, const float z )
    {
        yaw += y;
        pitch += x;

        if ( pitch > 89.0f )
        {
            pitch = 89.0f;
        }
        if ( pitch < -89.0f )
        {
            pitch = -89.0f;
        }

        updateFront();
    }

    void Translate( const float x, const float y, const float z )
    {
        const float scale = 0.05f;
        if ( x > 0.001f || x < -0.001f )
        {
            if ( x > 0.0f )
            {
                position.vx += (long)(right.vx*scale);
                position.vy += (long)(right.vy*scale);
                position.vz += (long)(right.vz*scale);
            }
            else
            {
                position.vx -= (long)(right.vx*scale);
                position.vy -= (long)(right.vy*scale);
                position.vz -= (long)(right.vz*scale);
            }
        }
        else if ( z > 0.001f || z < -0.001f )
        {
            if ( z > 0.0f )
            {
                position.vx -= (long)(front.vx*scale);
                position.vy -= (long)(front.vy*scale);
                position.vz -= (long)(front.vz*scale);
            }
            else
            {
                position.vx += (long)(front.vx*scale);
                position.vy += (long)(front.vy*scale);
                position.vz += (long)(front.vz*scale);
            }
        }
    }

    inline void UpdateMat()
    {
        static VECTOR at;
        at.vx = position.vx + front.vx;
        at.vy = position.vy + front.vy;
        at.vz = position.vz + front.vz;

        LookAt( &position, &at, &up, &viewMat );
    }

private:

    float pitch;
    float yaw;
    float FOV; // not used currently

    VECTOR position;
    VECTOR front;
    SVECTOR up;
    VECTOR right;

    MATRIX viewMat;

    // taken from psn00bsdk examples
    inline void cross_product(SVECTOR *v0, SVECTOR *v1, VECTOR *out)
    {
        out->vx = ((v0->vy*v1->vz)-(v0->vz*v1->vy))>>12;
        out->vy = ((v0->vz*v1->vx)-(v0->vx*v1->vz))>>12;
        out->vz = ((v0->vx*v1->vy)-(v0->vy*v1->vx))>>12;
    }
    inline void cross_product(VECTOR *v0, SVECTOR *v1, VECTOR *out)
    {
        out->vx = ((v0->vy*v1->vz)-(v0->vz*v1->vy))>>12;
        out->vy = ((v0->vz*v1->vx)-(v0->vx*v1->vz))>>12;
        out->vz = ((v0->vx*v1->vy)-(v0->vy*v1->vx))>>12;
    }

    // taken from psn00bsdk examples
    inline void LookAt( VECTOR *eye, VECTOR *at, SVECTOR *up, MATRIX *mtx )
    {
        VECTOR taxis;
        SVECTOR zaxis;
        SVECTOR xaxis;
        SVECTOR yaxis;
        VECTOR pos;
        VECTOR vec;

        setVector(&taxis, at->vx-eye->vx, at->vy-eye->vy, at->vz-eye->vz);
        VectorNormalS(&taxis, &zaxis);
        cross_product(&zaxis, up, &taxis);
        VectorNormalS(&taxis, &xaxis);
        cross_product(&zaxis, &xaxis, &taxis);
        VectorNormalS(&taxis, &yaxis);

        mtx->m[0][0] = xaxis.vx;	mtx->m[1][0] = yaxis.vx;	mtx->m[2][0] = zaxis.vx;
        mtx->m[0][1] = xaxis.vy;	mtx->m[1][1] = yaxis.vy;	mtx->m[2][1] = zaxis.vy;
        mtx->m[0][2] = xaxis.vz;	mtx->m[1][2] = yaxis.vz;	mtx->m[2][2] = zaxis.vz;

        pos.vx = -eye->vx;
        pos.vy = -eye->vy;
        pos.vz = -eye->vz;

        ApplyMatrixLV(mtx, &pos, &vec);
        TransMatrix(mtx, &vec);
    }

    // degrees to radians
    inline float deg2rad( float deg )
    {
        return deg * (3.1415926535f / 180.0f);
    }

    // degrees to psx fixed point (4096 = 360 deg)
    inline int deg2fixed( float deg )
    {
        return (int)(ONE * ( deg / 360.0f ));
    }

    inline void updateFront()
    {
        static VECTOR unnormalizedFront;

        int fixedYaw = deg2fixed( yaw );
        int fixedPitch = deg2fixed( pitch );

        int cosYaw = rcos( fixedYaw );
        int cosPitch = rcos( fixedPitch );
        int sinYaw = rsin( fixedYaw );
        int sinPitch = rsin( fixedPitch );

        unnormalizedFront.vx = (cosYaw * cosPitch)>>12;
        unnormalizedFront.vy = sinPitch;
        unnormalizedFront.vz = (sinYaw * cosPitch)>>12;

        VectorNormal( &unnormalizedFront, &front );

        cross_product( &front, &up, &right );
        VectorNormal( &right, &right );
    }
};

#endif // CAMERA_H_INCLUDED

