/*
 * Light.cpp
 */

#include "Light.h"

/*
===================
Constructor/Deconstructor
===================
*/
Light::Light()
{
    setVector( &pos, DefaultPos.vx, DefaultPos.vy, DefaultPos.vz );
    setVector( &angle, DefaultAngle.vx, DefaultAngle.vy, DefaultAngle.vz );
    setVector( &color, DefaultColor.vx, DefaultColor.vy, DefaultColor.vz );
}

Light::~Light()
{
}

/*
===================
Move the light position
===================
*/

void Light::Translate( int x, int y, int z )
{
    pos.vx += x;
    pos.vy += y;
    pos.vz += z;
}

void Light::TranslateTo( int x, int y, int z )
{
    pos.vx = x;
    pos.vy = y;
    pos.vz = z;
}

/*
===================
Setters
===================
*/
void Light::Rotate(   SVECTOR* angle )
{
    this->angle.vx += angle->vx;
    this->angle.vy += angle->vy;
    this->angle.vz += angle->vz;
}
void Light::RotateTo( SVECTOR* angle )
{
    this->angle.vx = angle->vx;
    this->angle.vy = angle->vy;
    this->angle.vz = angle->vz;
}

void Light::Rotate(   int x, int y, int z )
{
    angle.vx += x;
    angle.vy += y;
    angle.vz += z;
}
void Light::RotateTo( int x, int y, int z )
{
    angle.vx = x;
    angle.vy = y;
    angle.vz = z;
}

void Light::SetColor( int r, int g, int b )
{
    color.vx = r;
    color.vy = g;
    color.vz = b;
}

/*
===================
Getters
===================
*/
SVECTOR& Light::GetAngle(void)
{
    return angle;
}

/*
===================
Default light data
===================
*/
VECTOR  Light::DefaultColor = { ONE*3/4, ONE*3/4, ONE*3/4, 0 }; // ONE*3/4
SVECTOR Light::DefaultAngle = { 1024, -512, 1024, 0 };
SVECTOR Light::DefaultPos   = { ONE, ONE, ONE, 0 };
