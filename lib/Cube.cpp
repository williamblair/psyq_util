/*
 * Cube.cpp
 */
#include <Cube.h>

#define CUBESIZ 128

// Cube data
SVECTOR Cube::P0 = {-CUBESIZ/2,-CUBESIZ/2,-CUBESIZ/2,0};
SVECTOR Cube::P1 = { CUBESIZ/2,-CUBESIZ/2,-CUBESIZ/2,0};
SVECTOR Cube::P2 = { CUBESIZ/2, CUBESIZ/2,-CUBESIZ/2,0};
SVECTOR Cube::P3 = {-CUBESIZ/2, CUBESIZ/2,-CUBESIZ/2,0};

SVECTOR Cube::P4 = {-CUBESIZ/2,-CUBESIZ/2, CUBESIZ/2,0};
SVECTOR Cube::P5 = { CUBESIZ/2,-CUBESIZ/2, CUBESIZ/2,0};
SVECTOR Cube::P6 = { CUBESIZ/2, CUBESIZ/2, CUBESIZ/2,0};
SVECTOR Cube::P7 = {-CUBESIZ/2, CUBESIZ/2, CUBESIZ/2,0};

// ONE is defined in libgte.h as 4096; as that
// equals 360 degrees for the gte
SVECTOR Cube::N0 = { ONE,   0,    0, 0};
SVECTOR Cube::N1 = {-ONE,   0,    0, 0};
SVECTOR Cube::N2 = {0,    ONE,    0, 0};
SVECTOR Cube::N3 = {0,   -ONE,    0, 0};
SVECTOR Cube::N4 = {0,      0,  ONE, 0};
SVECTOR Cube::N5 = {0,      0, -ONE, 0};


// vertices list
SVECTOR Cube::cubeVertices[6*4] = {
	Cube::P0,Cube::P1,Cube::P2,Cube::P3,
	Cube::P1,Cube::P5,Cube::P6,Cube::P2,
	Cube::P5,Cube::P4,Cube::P7,Cube::P6,
	Cube::P4,Cube::P0,Cube::P3,Cube::P7,
	Cube::P4,Cube::P5,Cube::P1,Cube::P0,
	Cube::P6,Cube::P7,Cube::P3,Cube::P2
};

// normal list
SVECTOR Cube::cubeNormals[6] = {
	Cube::N5, Cube::N0, Cube::N4, Cube::N1, Cube::N3, Cube::N2
};

#undef CUBESIZ

