/*
 * Cube.h
 */

#ifndef CUBE_H_INCLUDED
#define CUBE_H_INCLUDED

#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <Model.h>

/*
===============================================================================

Cube

    Model type with already defined vertices/normals
===============================================================================
*/
class Cube : public Model
{
public:

    Cube() {
        printf("Cube constructor!\n");

        this->vertices = this->cubeVertices;
        this->normals = this->cubeNormals;

        this->numVertices = 6*4;
        this->numNormals = 6;
    }

    virtual ~Cube() {}

private:

    // cube points
    static SVECTOR P0;
    static SVECTOR P1;
    static SVECTOR P2;
    static SVECTOR P3;
    static SVECTOR P4;
    static SVECTOR P5;
    static SVECTOR P6;
    static SVECTOR P7;

    // cube normals vectors
    static SVECTOR N0;
    static SVECTOR N1;
    static SVECTOR N2;
    static SVECTOR N3;
    static SVECTOR N4;
    static SVECTOR N5;
    static SVECTOR N6;
    static SVECTOR N7;

    // vertices - pointers to coordinates
    static SVECTOR cubeVertices[6 * 4];

    // normals - pointers to normals vectors
    static SVECTOR cubeNormals[6];
};

#endif // PSX_RENDERER_H_INCLUDED

