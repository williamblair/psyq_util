#ifndef MD2_MODEL_H_INCLUDED
#define MD2_MODEL_H_INCLUDED

#include <array.hpp>
#include <Aabb.h>

// TODO - replace this
typedef struct Color4f
{
    float r,g,b,a;
} Color4f;

// ASM optimized packing of a DrawVertex:
static inline void packDrawVertex(/*DrawVertex & dv,*/
                                  const PSXVector & position, 
                                  const PSXVector & texCoord, 
                                  const PSXVector & color)
{
#if 0
    asm volatile (
        "lqc2    vf4, 0x0(%1)  \n\t" // vf4 = position
        "lqc2    vf5, 0x0(%2)  \n\t" // vf5 = texCoord
        "lqc2    vf6, 0x0(%3)  \n\t" // vf6 = color
        "vmove.w vf4, vf0      \n\t" // vf4.w = 1.0 (set position.w to 1)
        "sqc2    vf6, 0x20(%0) \n\t" // dv.color    = vf6
        "sqc2    vf5, 0x10(%0) \n\t" // dv.texCoord = vf5
        "sqc2    vf4, 0x00(%0) \n\t" // dv.position = vf4
        : : "r" (&dv), "r" (&position), "r" (&texCoord), "r" (&color)
    );
#endif
}

// forward declaration
class Renderer;

// TODO - make these calculations fixed point instead of float
class Md2AnimState
{
public:

    friend class Renderer;

    u_int startFrame;
    u_int endFrame; // last frame, inclusive (<=)
    u_int fps; // animation frames per second
    u_int curFrame;
    u_int nextFrame;
    float curTime;
    float oldTime;
    float interp; // percent of interpolation for this animation
    
    Md2AnimState()
    {
        Clear();
    }
    
    // should be called every frame with the current system time 
    // in seconds
    void Update(float timeSeconds, u_int keyframeCount)
    {
        curTime = timeSeconds;
        const u_int maxFrame = keyframeCount - 1;
        
        // calculate current and next frames
        if ((curTime - oldTime) > (1.0 / fps))
        {
            curFrame = nextFrame;
            nextFrame++;
            
            if (nextFrame > endFrame)
            {
                nextFrame = startFrame;
            }
            
            oldTime = curTime;
        }
        
        // prevent having current/next frame greater than total frames
        if (curFrame > maxFrame)
        {
            curFrame = 0;
        }
        if (nextFrame > maxFrame)
        {
            nextFrame = 0;
        }
        
        interp = fps * (curTime - oldTime);
    }
    
    void Clear()
    {
        startFrame = 0;
        endFrame = 0;
        fps = 0;
        curFrame = 0;
        nextFrame = 0;
        curTime = 0;
        oldTime = 0;
        interp = 0;
    }
};

// Used for non-standard animations.
const u_int MD2ANIM_DEFAULT_FPS = 7;

// Standard MD2 animations, with predefined frame numbers and FPS.
enum StdMd2AnimId
{
    MD2ANIM_STAND,
    MD2ANIM_RUN,
    MD2ANIM_ATTACK,
    MD2ANIM_PAIN_A,
    MD2ANIM_PAIN_B,
    MD2ANIM_PAIN_C,
    MD2ANIM_JUMP,
    MD2ANIM_FLIP,
    MD2ANIM_SALUTE,
    MD2ANIM_FALL_BACK,
    MD2ANIM_WAVE,
    MD2ANIM_POINT,
    MD2ANIM_CROUCH_STAND,
    MD2ANIM_CROUCH_WALK,
    MD2ANIM_CROUCH_ATTACK,
    MD2ANIM_CROUCH_PAIN,
    MD2ANIM_CROUCH_DEATH,
    MD2ANIM_DEATH_FALL_BACK,
    MD2ANIM_DEATH_FALL_FORWARD,
    MD2ANIM_DEATH_FALL_BACK_SLOW,
    MD2ANIM_BOOM,

    // Number of entries in this enum. Internal use.
    MD2ANIM_COUNT
};

class Md2Model
{
public:

    friend class Renderer;

    Md2Model();

    // Load from a MD2 file data stored in memory. The memory is reference so it
    // needs to exist for the lifetime of this object
    bool InitFromMemory(const u_char* data, u_int sizeBytes, float scale = 1.0f);

    void SetAnim( StdMd2AnimId animId );
    
    void UpdateAnim( float timeSeconds ) { animState.Update( timeSeconds, md2FrameCount ); }
    
    // Generates a vertex array for the given animation frame. drawVerts[] must be triangleCount*3 size
    void AssembleFrame(u_int frameIndex, 
                        u_int skinWidth, u_int skinHeight, 
                        const Color4f& vertColor/*, 
                        DrawVertex* drawVerts*/) const;
    
    // Generate vertex array for given frame, from the compressed MD2 frame data.
    // drawVerts[] must be triangleCount*3 size
    void AssembleFrameInterpolated(u_int frameA, u_int frameB, 
                                    float interp, 
                                    u_int skinWidth, u_int skinHeight, 
                                    const Color4f& vertColor/*, 
                                    DrawVertex* drawVerts*/) const;
    
    // print list of animations to the console
    void PrintAnimList() const
    {
        printf("|  md2-anim-name  | start |  end  |\n");
        for (u_int i = 0; i < md2Anims.size(); i++)
        {
            printf("| %-15s | %-5u | %-5u |\n",
                    md2Anims[i].name,
                    md2Anims[i].start,
                    md2Anims[i].end);
        }
        printf("Listed %u animations\n", md2Anims.size());
    }

    // find animation by exact name. fps might be an estimate
    bool FindAnimByName(const char* name, u_int& start, u_int& end, u_int& fps) const;

    // find animation by partial name. fps might be an estimate
    bool FindAnimByPartialName(const char* partialName, u_int& start, u_int& end, u_int& fps) const;

    // get animation by its index in the anim list. index range is 0 to getAnimCount() - 1
    bool FindAnimByIndex(u_int index, u_int& start, u_int& end, u_int& fps) const;
    
    // Get the range and frame rate of a standard MD2 animation
    static void GetStdAnim(StdMd2AnimId id, u_int& start, u_int& end, u_int& fps)
    {
        // assert(id >= 0 && id < MD2ANIM_COUNT);
        start = stdMd2Anims[id].start;
        end = stdMd2Anims[id].end;
        fps = stdMd2Anims[id].fps;
    }

    // Get the frame rate of an animation if it is in within a known range; default val otherwise
    static u_int GetAnimFpsForRange(u_int start, u_int end)
    {
        for (u_int i = 0; i < MD2ANIM_COUNT; i++)
        {
            if (start == stdMd2Anims[i].start &&
                end == stdMd2Anims[i].end)
            {
                return stdMd2Anims[i].fps;
            }
        }
        return MD2ANIM_DEFAULT_FPS;
    }

    // Bounding box for a given frame
    const Aabb& GetBoundsForFrame(u_int frameIndex) const
    {
        return md2FrameBounds[frameIndex];
    }

    u_int GetKeyframeCount() const { return md2FrameCount;                     }
    u_int GetVertexCount() const   { return md2FrameCount * md2VertesPerFrame; }
    u_int GetTriangleCount() const { return md2TriangleCount;                  }
    u_int GetAnimCount() const     { return md2Anims.size();                   }
    float GetModelScale() const    { return md2Scale;                          }

//private:

    Md2Model(const Md2Model& );
    Md2Model & operator=(const Md2Model& );
    
    // MD2 constants
    static const u_int MD2_GOOD_VERSION   = 8; // Version used in Quake II
    static const u_int MD2_MAX_VERTS      = 2048; // per keyframe
    static const u_int MD2_MAX_TEXCOORDS  = 2048; // for the whole model
    static const u_int MD2_MAX_TRIS       = 4096; // for the whole model
    static const u_int MD2_MAX_KEYFRAMES  = 512; // for the whole model
    static const u_int MD2_MAX_SKINS      = 32; // for the whole model
    static const u_int MD2_MAX_NAME_CHARS = 64; // skins/anims
    
    // MD2 Header
    struct __attribute__((packed)) Header
    {
        u_int magic;
        u_int version;
        u_int skinWidth;
        u_int skinHeight;
        u_int frameSize;        // size of a single frame in bytes
        u_int skinCount;
        u_int vertsPerFrame;
        u_int texCoordCount;
        u_int triangleCount;
        u_int glCmdCount;
        u_int frameCount;       
        u_int offsetSkins;      // offset to skin data
        u_int offsetTexCoords;  // offset to texture coords
        u_int offsetTris;       // offset to triangle data
        u_int offsetFrames;     // offset to frame data
        u_int offsetGLCmds;     // offset to OpenGL commands
        u_int offsetEnd;        // offset to the end of the file
    };
    
    // skin texture filename
    struct __attribute__((packed)) Skin
    {
        char name[MD2_MAX_NAME_CHARS]; // null terminated filename str
    };
    
    // texture coordinates (uv)
    struct __attribute__((packed)) TexCoord
    {
        u_short u, v;
    };
    
    // Triangle data
    struct __attribute__((packed)) Triangle
    {
        u_short vertex[3]; // trianglex vertex indices
        u_short uv[3]; // texture coord indices
    };
    
    // Vertex data
    struct __attribute__((packed)) Vertex
    {
        u_char v[3]; // compressed vertex position
        u_char normalIndex; // normal vector index into emblematic MD2 normals table
    };
    
    // Keyframe data
    struct __attribute__((packed)) Keyframe
    {
        float scale[3]; // scale factors
        float translate[3]; // translation vector
        char name[16]; // frame name
        
        // followed by a vertex packet:
        // Vertex frameVerts[header.vertsPerFrame];
    };
    
    // animation info (frame indices and anim name):
    struct __attribute__((packed)) Anim
    {
        u_int start, end;
        char name[MD2_MAX_NAME_CHARS];
    };
    
    // Internal helpers
    
    void SetupAnimations()
    {
        Anim animInfo;
        char frameAnim[MD2_MAX_NAME_CHARS * 2];
        char currentAnim[MD2_MAX_NAME_CHARS * 2];
        
        memset(&animInfo, 0, sizeof(animInfo));
        memset(currentAnim, 0, sizeof(currentAnim));
        
        md2Anims.reserve(md2FrameCount);
        for (u_int i = 0; i < md2FrameCount; i++)
        {
            memset(frameAnim, 0, sizeof(frameAnim));
            const char* frameName = GetKeyframe(i).name;
            
            // extract the animation name from frame name:
            // MD2 anim names are in the form <animName><frameNum>, like "death001"
            //
            // strcspn() will find the index of the first occurance of any number in the frame name
            size_t len = strcspn(frameName, "0123456789");
            if ((strlen(frameName) - 3 == len) && (frameName[len] != '\0'))
            {
                ++len;
            }
            
            strncpy(frameAnim, frameName, len);
            
            if (strcmp(currentAnim, frameAnim) != 0)
            {
                if (i > 0)
                {
                    // prev anim finished, store it and start new
                    strncpy(animInfo.name, currentAnim, sizeof(animInfo.name));
                    md2Anims.pushBack(animInfo);
                }
                
                // initialize new anim info
                animInfo.start = i;
                animInfo.end = i;
                strncpy(currentAnim, frameAnim, sizeof(currentAnim));
            }
            else
            {
                animInfo.end = i;
            }
        }
        
        // add the last animation
        strncpy(animInfo.name, currentAnim, sizeof(animInfo.name));
        md2Anims.pushBack(animInfo);
    }
    
    void ComputeAabbForFrame(u_int frameIndex)
    {
        Aabb& frameAabb = md2FrameBounds[frameIndex];
        frameAabb.Clear();
        
        const Keyframe& keyframe = GetKeyframe(frameIndex);
        
        for (u_int t = 0; t < md2TriangleCount; t++)
        {
            for (u_int v = 0; v < 3; v++)
            {
                const Vertex& vert = GetFrameVertex(frameIndex, md2Triangles[t].vertex[v]);
                
                // uncompress vertex position and transform it, with swapping y/z
                PSXVector xyzPos;
                xyzPos.x = float2fixed((keyframe.scale[0] * vert.v[0] + keyframe.translate[0]) * md2Scale);
                xyzPos.z = float2fixed((keyframe.scale[1] * vert.v[1] + keyframe.translate[1]) * md2Scale);
                xyzPos.y = float2fixed((keyframe.scale[2] * vert.v[2] + keyframe.translate[2]) * md2Scale);
                xyzPos.w = ONE;
                
                // Add to bounds if new min/max
                frameAabb.mins = min3PerElement(xyzPos, frameAabb.mins);
                frameAabb.maxs = max3PerElement(xyzPos, frameAabb.maxs);
            }
        }
    }
    const Keyframe& GetKeyframe(u_int frameIndex) const
    {
        //assert(frameIndex < md2FrameCount)
        
        const u_char* framesPtr = (const u_char*)(md2Keyframes);
        
        // each keyframe is a single `Keyframe` struct + a vertex packet with md2VertesPerFrame vertices
        const u_int keyframePacketSize = sizeof(Keyframe) + (sizeof(Vertex) * md2VertesPerFrame);
        
        // skip to the desired frame
        return *((const Keyframe*)(framesPtr + (frameIndex * keyframePacketSize)));
    }
    inline const Vertex& GetFrameVertex(u_int frameIndex, u_int vertexIndex) const
    {
        // assert(frameIndex < md2FrameCount);
        
        const u_char* vertsPtr = (u_char*)md2Keyframes;
        
        // each keyframe is a single 'Keyframe' struct + a vertex packet with md2VertsPerFrame vertices
        const u_int keyframePacketSize = sizeof(Keyframe) + (sizeof(Vertex) * md2VertesPerFrame);
        
        // skip to the desired frame
        vertsPtr += frameIndex * keyframePacketSize;
        
        // move forward to the vertices
        vertsPtr += sizeof(Keyframe);
        
        return ((const Vertex*)(vertsPtr))[vertexIndex];
    }
    
private:

    // extension data pointers
    const Header*   md2Header;
    const Skin*     md2Skins;
    const TexCoord* md2TexCoords;
    const Triangle* md2Triangles;
    const Keyframe* md2Keyframes;
    
    // cache for quick access
    u_int md2TriangleCount;
    u_int md2VertesPerFrame;
    u_int md2FrameCount;
    
    // scaling applied to model vertices; 1.0 by default
    float md2Scale;

    // assuming the model has animation data
    Md2AnimState animState;
    
    // Data owned by Md2Model
    Array<Anim> md2Anims;
    Array<Aabb> md2FrameBounds;
    
    static u_int chars2uint(char a, char b, char c, char d)
    {
        return (((u_int)a) << 0) |
               (((u_int)b) << 8) |
               (((u_int)c) << 16) |
               (((u_int)d) << 24);
    }
    
    struct Md2Anim
    {
        u_char start;
        u_char end;
        u_char fps;
    };
    static Md2Anim stdMd2Anims[MD2ANIM_COUNT];
};


#endif // MD2_MODEL_H_INCLUDED

