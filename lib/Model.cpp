#include "Model.h"

Model::Model() :
    vertices(NULL),
    normals(NULL),
    indices(NULL),
    texCoords(NULL),
    texture(NULL),
    numVertices(0),
    numNormals(0),
    numIndices(0)

{
    setVector(&rotation, 0,0,0);
    setVector(&position, 0,0,0);

    UpdateModelMatrix();
}

Model::~Model()
{
}

void Model::Rotate(int x, int y, int z)
{
    rotation.vx += x;
    rotation.vy += y;
    rotation.vz += z;

    UpdateModelMatrix();
}

void Model::Translate(int x, int y, int z)
{
    position.vx += x;
    position.vy += y;
    position.vz += z;

    UpdateModelMatrix();
}

void Model::Scale(int s)
{
    scale.vx = s;
    scale.vy = s;
    scale.vz = s;
}

