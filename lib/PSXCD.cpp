#include <PSXCD.h>
#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <libcd.h>
#include <libapi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

bool ReadFile( char* fileName, char* inBuf )
{
    char* buf = nullptr; // store the read file
    CdlFILE fileStruct;
    int i;
    int cnt;
    memset( &fileStruct, 0, sizeof(fileStruct) );

    // check disk status
    u_char result[8];
    CdControl( CdlNop, 0, result );
    if ( result[0] & CdlStatShellOpen )
    {
        if ( CdDiskReady(1) != CdlComplete )
        {
            printf("Shell open waiting\n");
            while ( CdDiskReady(0) != CdlComplete );
        }
    }

    // name MUST be uppsercase in the form "\\THE\\FILE\\PATH.EXT;1"
    unsigned long bufSize = 0;
    if ( CdSearchFile( &fileStruct, fileName ) == 0 )
    {
        printf( "CD search file failed (pos: 0x%X)\n", fileStruct.pos );
        return false;
    }
    else
    {
        printf( "Found file: pos,size,name: 0x%08X, %lu, %s\n",
                *((u_int*)&fileStruct.pos),
                fileStruct.size,
                fileStruct.name );
        const unsigned long sectorSize = 2048;
        unsigned long bufSize = fileStruct.size + (sectorSize - (fileStruct.size % sectorSize));
        buf = new char[bufSize];
    }


    // start the read operation
    // num bytes (size) must be a multiple of 2048
    //      reads entire file if num bytes is 0
    CdReadFile( fileName, (u_long*)buf, 0 ); 

    // wait for read operation to finish
    while (( cnt = CdReadSync(1, 0) ) > 0 );
    if ( cnt == -1 ) {
        printf( "CdReadSync error\n" );
        delete[] buf;
        return false;
    }
    memcpy( inBuf, buf, fileStruct.size );
    delete[] buf;
    buf = nullptr;

    return true;
}
