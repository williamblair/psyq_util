/*
 * Renderer.h
 */

#ifndef PSX_RENDERER_H_INCLUDED
#define PSX_RENDERER_H_INCLUDED

#include "Camera.h"
#include "Font.h"
#include "Texture.h"
#include "Light.h"
#include "Md2Model.h"
#include "Model.h"
#include "Sprite.h"
#include <libcd.h>
#include <libapi.h>

/*
===============================================================================

Renderer

    PSX Rendering Implementation
===============================================================================
*/
class Renderer/* : public Renderer*/
{
public:

    Renderer();
    virtual ~Renderer();

    void Init();
    void Update();
    void DrawModel( const Model& model );
    void DrawMd2Model( Md2Model& model, PSXMatrix& modelMat, Texture& modelTex );

    inline void DrawSprite( const Sprite& sprite )
    {
        // draw at depth zero (always on top)
        addPrim( &curBuffer->ot[0], &sprite.poly );
    }

    inline void SetCamera( Camera& camera )
    {
        this->camera = &camera;
    }

    inline void SetTexture( Texture& texture )
    {
        this->tpageId = texture.GetTexturePageId();
    }

    inline void AddLight( Light& light )
    {
        this->lights[numLights++] = &light;
    }

    void DrawString( Font& font, int x, int y, const char* msg );

    inline void printLightPosMat()
    {
        printf("%d, %d, %d\n%d, %d, %d\n%d, %d, %d\n",
                lightMatrices.pos.m[0][0], lightMatrices.pos.m[0][1], lightMatrices.pos.m[0][2],
                lightMatrices.pos.m[1][0], lightMatrices.pos.m[1][1], lightMatrices.pos.m[1][2],
                lightMatrices.pos.m[2][0], lightMatrices.pos.m[2][1], lightMatrices.pos.m[2][2]);
    }

    inline void printLightColMat()
    {
        printf("%d, %d, %d\n%d, %d, %d\n%d, %d, %d\n",
                lightMatrices.color.m[0][0], lightMatrices.color.m[0][1], lightMatrices.color.m[0][2],
                lightMatrices.color.m[1][0], lightMatrices.color.m[1][1], lightMatrices.color.m[1][2],
                lightMatrices.color.m[2][0], lightMatrices.color.m[2][1], lightMatrices.color.m[2][2]);
    }

private:

    static const size_t OT_LEN = 4096;
    static const size_t PACKET_LEN = 8192*8;
    static const size_t SCREEN_WIDTH = 320;
    static const size_t SCREEN_HEIGHT = 240;

    struct displayBuffer_t {
        DISPENV disp;
        DRAWENV draw;

        unsigned int ot[OT_LEN];
        char         p [PACKET_LEN];
    };
    struct displayBuffer_t buffers[2];
    displayBuffer_t*       curBuffer;

    Light* lights[3];
    struct LightMatrices {
        MATRIX angle;
        MATRIX pos;
        MATRIX color;
    };
    struct LightMatrices lightMatrices;
    SVECTOR lightAngle;
    u_char numLights;
    Camera* camera;

    char* nextPrim;

    u_short tpageId;

    inline void loadDefaultTexture()
    {
        Texture::LoadDefault();
        tpageId = Texture::DefaultTexture.tpage_id;
    }

    void RenderWithIndices( const Model& model );
    void RenderMd2Model( Md2Model& model, Texture& modelTex );
    void RenderMd2ModelAnimated( Md2Model& model, Texture& modelTex );
    void RenderWithVertices( const Model& model );
    void updateLights();
    inline void shortLerpScale(SVECTOR& res, VECTOR& v1, VECTOR& v2, long t, long s)
    {
#define fixedMul(a,b) (((a)*(b)) >> 12)
    	res.vx = fixedMul((v1.vx + fixedMul((t), (v2.vx - v1.vx))), s);
    	res.vy = fixedMul((v1.vy + fixedMul((t), (v2.vy - v1.vy))), s);
    	res.vz = fixedMul((v1.vz + fixedMul((t), (v2.vz - v1.vz))), s);
    	//res.vx  = fixedMul(res.vx, s);
    	//res.vy  = fixedMul(res.vy, s);
	    //res.vz  = fixedMul(res.vz, s);
#undef fixedMul
    }

};

#endif // PSX_RENDERER_H_INCLUDED

