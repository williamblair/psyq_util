/*
 * Font.cpp
 */

#include "Font.h"

/*
===================
Constructor/Deconstructor
===================
*/
Font::Font()
{
    fontWidth = 0;
    fontHeight = 0;

    charsPerRow = 0;
}

Font::~Font()
{
}

/*
===================
Parameters sent to the texture to load,
See texture class for details
===================
*/
void Font::LoadTexture(
    u_long* texdata,  // pointer to texture data
    u_long* clutdata, // pointer to CLUT data (NULL if none)
    TEXTURE_BPP bpp,  // 0 - 16bit, 1 - 8bit, 2 - 4 bit
    int x,        // where in VRAM to load the texture
    int y,
    int w,        // size of the texture
    int h,
    int clutX,    // where to upload the clut data (if given)
    int clutY
)
{
    texture.Load(
        texdata,  // pointer to texture data
        clutdata, // pointer to CLUT data (NULL if none)
        bpp,  // 0 - 16bit, 1 - 8bit, 2 - 4 bit
        x,        // where in VRAM to load the texture
        y,
        w,        // size of the texture
        h,
        clutX,    // where to upload the clut data (if given)
        clutY
    );
}

void Font::SetWidthAndHeight( float width, float height )
{
    fontWidth = width;
    fontHeight = height;

    if ( fontWidth > 0.0f ) {
        charsPerRow = (u_int)(texture.GetWidth() / fontWidth);
    }
}

/*
===================
Draw a string of characters using the internal texture
===================
*/
/*
void Font::DrawString( u_int x, u_int y, char* message )
{
    size_t   i;
    POLY_FT4 sprite;
    u_int    u = 0;
    u_int    v = 0;
    size_t   message_length = strlen(message);

    SetPolyFT4(&sprite);
    SetShadeTex(&sprite, 1); // shadetex disable

    setUVWH(&sprite, 0,0, fontWidth, fontHeight);
    setXYWH(&sprite, x,y, fontWidth, fontHeight);

    sprite.tpage = texture.get_texture_page_id();
    sprite.clut  = texture.get_clut_id();

    for ( i = 0; i < message_length; ++i )
    {
        x += fontWidth;

        if ( message[i] != ' ' && (u_int)message[i] >= 97 ) {
            calc_char_u_v( message[i], &u, &v );

            setUVWH(&sprite, u,v, fontWidth, fontHeight);
            setXYWH(&sprite, x,y, fontWidth, fontHeight);

            DrawPrim(&sprite);
        }
    }
}
*/



