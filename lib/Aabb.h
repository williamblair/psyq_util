#ifndef AABB_H_INCLUDED
#define AABB_H_INCLUDED

#include <libgte.h>
#include <PSXMatrix.h>
#include <PSXVector.h>

#define fixedMul(a,b) (((a)*(b))>>12)
// TODO - move into header or something
//static inline long float2fixed(const float a)
//{
//    return (long)(a * 4096);
//}

// simple axis aligned bounding box
// Has a min point and a max point for the two extents of a box
struct Aabb
{
    inline Aabb() :
        mins(0, 0, 0, ONE),
        maxs(0, 0, 0, ONE)
    {
    }
    inline Aabb(const long min[], const long max[]) :
        mins(min[0], min[1], min[2], ONE),
        maxs(max[0], max[1], max[2], ONE)
    {
    }
    inline Aabb(const PSXVector& min, const PSXVector& max) :
        mins(min),
        maxs(max)
    {
    }
    inline Aabb(const void* vertices, u_int vertexCount, u_int vertexStride)
    {
        FromMeshVertices(vertices, vertexCount, vertexStride);
    }
    
    // Inside out bounds
    inline void Clear()
    {
        const long INF = 2147483647; // max size of long int
        mins = PSXVector(INF, INF, INF, ONE);
        maxs = PSXVector(-INF, -INF, -INF, ONE);
    }
    
    // Set as a point at the origin
    inline void SetZero()
    {
        mins = PSXVector(0, 0, 0, ONE);
        maxs = PSXVector(0, 0, 0, ONE);
    }
    
    // Returns true if bounds are inside out
    inline bool IsCleared() const
    {
        return (mins.x > maxs.x);
    }
    
    // returns the volume of the bounds
    inline long GetVolume() const
    {
        if ((mins.x >= maxs.x) || (mins.y >= maxs.y) || (mins.z >= maxs.z))
        {
            return 0;
        }
        return (fixedMul( fixedMul((maxs.x - mins.x), (maxs.y - mins.y)), 
                         (maxs.z - mins.z) ));
    }
    
    inline PSXVector GetCenter() const
    {
        return ((maxs + mins) / float2fixed(0.5f));
    }
    
    // Test if a point is touching or inside the box bounds
    inline bool ContainsPoint(const PSXVector& boxPos, const PSXVector& p)
    {
        if (p.x < mins.x + boxPos.x ||
            p.x > maxs.x + boxPos.x ||
            p.y < mins.y + boxPos.y ||
            p.y > maxs.y + boxPos.y ||
            p.z < mins.z + boxPos.z ||
            p.z > maxs.z + boxPos.z)
        {
            return false;
        }
        return true;
    }
    
    // returns true if the line intersects the bounds between the start and end points
    inline bool LineIntersection(const PSXVector& start, const PSXVector& end) const
    {
        const PSXVector center     = (mins + maxs) * float2fixed(0.5f);
        const PSXVector extents    = maxs - center;
        const PSXVector lineDir    = (end - start) * float2fixed(0.5f);
        const PSXVector lineCenter = start + lineDir;
        const PSXVector dir        = lineCenter - center;
        
        const long ld0 = abs(lineDir.x);
        if (abs(dir.x) > (extents.x + ld0))
        {
            return false;
        }
        
        const long ld1 = abs(lineDir.y);
        if (abs(dir.y) > (extents.y + ld1))
        {
            return false;
        }
        
        const long ld2 = abs(lineDir.z);
        if (abs(dir.z) > (extents.z + ld2))
        {
            return false;
        }
        
        const PSXVector vCross = crossProduct(lineDir, dir);
        if (abs(vCross.x) > ((extents.y * ld2 + extents.z * ld1) >> 12))
        {
            return false;
        }
        
        if (abs(vCross.y) > ((extents.x * ld2 + extents.z * ld0) >> 12))
        {
            return false;
        }
        
        if (abs(vCross.z) > ((extents.x * ld1 + extents.y * ld0) >> 12))
        {
            return false;
        }
        
        
        return true;
    }
    
    // Build the tightest bounds for a set of mesh vertices. 
    // First element of the vertex must be a Vec3 (the vertex position)
    inline Aabb& FromMeshVertices(const void* vertices, u_int vertexCount, u_int vertexStride)
    {
        // assert(vertices != NULL)
        // assert(vertexCount != 0 && vertexStride != 0);
        
        Clear();
        const u_char* ptr = (u_char*)vertices;
        for (u_int i = 0; i < vertexCount; i++)
        {
            const PSXVector* v = (const PSXVector*)(ptr + i * vertexStride);
            const PSXVector xyz(v->x, v->y, v->z, ONE);
            
            // selects the min/max x,y,z from each vector
            mins = min3PerElement(xyz, mins);
            maxs = max3PerElement(xyz, maxs);
        }
        
        return *this;
    }
    
    // Scale the internal vertices
    inline Aabb& Scale(long s)
    {
        mins *= s;
        maxs *= s;
        return *this;
    }
    
    // Transform the points defining this bounds by a given matrix
    inline Aabb& Transform(const PSXMatrix& mat)
    {
        PSXVector tmp;
        
        tmp = mat * mins;
        mins = tmp;
        
        tmp = mat * maxs;
        maxs = tmp;
        
        return *this;
    }
    
    // Get a transformed copy of this Aabb
    inline Aabb Transformed(const PSXMatrix& mat) const
    {
        Aabb dummy(*this);
        dummy.Transform(mat);
        return dummy;
    }
    
    // Turn the Aabb into a set of points defining a box
    inline void ToPoints(PSXVector points[8]) const
    {
        const PSXVector b[2] = { maxs, mins };
        for (int i = 0; i < 8; i++)
        {
            points[i].x = b[(i ^ (i >> 1)) & 1].x;
            points[i].y = b[(i >> 1) & 1].y;
            points[i].z = b[(i >> 2) & 1].z;
            points[i].w = ONE;
        }
    }
    
    // Linearly interpolate two Aabbs
    static inline Aabb Lerp(const Aabb& a, const Aabb& b, const long t)
    {
        PSXVector mins;
        mins.x = a.mins.x + fixedMul(t, (b.mins.x - a.mins.x));
        mins.y = a.mins.y + fixedMul(t, (b.mins.y - a.mins.y));
        mins.z = a.mins.z + fixedMul(t, (b.mins.z - a.mins.z));
        mins.w = ONE;
        
        PSXVector maxs;
        maxs.x = a.maxs.x + fixedMul(t, (b.maxs.x - a.maxs.x));
        maxs.y = a.maxs.y + fixedMul(t, (b.maxs.y - a.maxs.y));
        maxs.z = a.maxs.z + fixedMul(t, (b.maxs.z - a.maxs.z));
        maxs.w = ONE;
        
        return Aabb(mins, maxs);
    }
    
    // Test if two Aabbs collide/intersect
    static bool Collision(const Aabb& box1, const PSXVector& box1Pos, const Aabb& box2, const PSXVector& box2Pos)
    {
        if ((box1.mins.x + box1Pos.x) > (box2.maxs.x + box2Pos.x) ||
            (box1.maxs.x + box1Pos.x) < (box2.mins.x + box2Pos.x))
        {
            return false;
        }
        if ((box1.mins.y + box1Pos.y) > (box2.maxs.y + box2Pos.y) ||
            (box1.maxs.y + box1Pos.y) < (box2.mins.y + box2Pos.y))
        {
            return false;
        }
        if ((box1.mins.z + box1Pos.z) > (box2.maxs.z + box2Pos.z) ||
            (box1.maxs.z + box1Pos.z) < (box2.mins.z + box2Pos.z))
        {
            return false;
        }
        
        return true;
    }
    
    // Minimum and maximum extents of the box
    PSXVector mins;
    PSXVector maxs;
};

#undef fixedMul
#endif // AABB_H_INCLUDED

