/*
 * Sprite.cpp
 */

#include "Sprite.h"

Sprite::Sprite()
{
	width = 0; height = 0;
	clip_x = 0; clip_y = 0;

	tex_id = 0;
	clut_id = 0;

	anim_speed = 20;
	anim_count = 0;

    clut_x = 0; clut_y = 0;

    pos.vx = 0;
    pos.vy = 0;
}

Sprite::~Sprite()
{
}

// this should only happen once since the members are static
// vram_texture_x might need to variably change based on
// if the screen is
int Sprite::vram_texture_x = 320;
int Sprite::vram_texture_y = 0;

int Sprite::vram_clut_x = 0;
int Sprite::vram_clut_y = 500;

void Sprite::LoadTexture(
        u_long* texdata,  // pointer to texture data
        u_long* clutdata, // pointer to CLUT data (NULL if none)
        TEXTURE_BPP bpp,  // 0 - 16bit, 1 - 8bit, 2 - 4 bit
        int x,        // where in VRAM to load the texture
        int y,
        int w,        // size of the texture
        int h,
        int clutX,    // where to upload the clut data (if given)
        int clutY
)
{
    texture.Load(
        texdata,  // pointer to texture data
        clutdata, // pointer to CLUT data (NULL if none)
        bpp,      // 0 - 16bit, 1 - 8bit, 2 - 4 bit
        x,        // where in VRAM to load the texture
        y,
        w,        // size of the texture
        h,
        clutX,    // where to upload the clut data (if given)
        clutY
    );

	SetPolyFT4( &poly );
	SetShadeTex( &poly, 1 ); // shadtex disable

	tex_width = this->texture.GetWidth();
    tex_height = this->texture.GetHeight();

//    pos.vx = 0;
//    pos.vy = 0;

    tex_id = this->texture.GetTexturePageId();
    clut_id = this->texture.GetClutId();

	// set the texture ids in the polygon
	poly.tpage = tex_id;
	if ( clut_id != 0 && (short)clut_id != -1 )
    {
        poly.clut = clut_id;
    }

	// set the texture point
	setUVWH( &poly, 0,0, tex_width, tex_height );

	// set the location of the sprite
	//setXYWH(&poly, 0,0, tex_width, tex_height);

    //pos.vx = x;
    //pos.vy = y;

    width = tex_width;
    height = tex_height;

	setXYWH( &poly, pos.vx, pos.vy, width, height );
	setUVWH( &poly, clip_x, clip_y, width, height );
}

/*
==================
Use an existing texture
==================
*/
void Sprite::SetTexture( Texture& texture )
{

    this->texture.CopyFrom( texture );

	SetPolyFT4( &poly );
	SetShadeTex( &poly, 1 ); // shadtex disable

	tex_width = this->texture.GetWidth();
    tex_height = this->texture.GetHeight();

	//pos.vx = 0;
    //pos.vy = 0;

    tex_id = this->texture.GetTexturePageId();
    clut_id = this->texture.GetClutId();

	// set the texture ids in the polygon
	poly.tpage = tex_id;
	if ( clut_id != 0 && (short)clut_id != -1 )
    {
        poly.clut = clut_id;
    }

	// set the texture point
	setUVWH( &poly, 0,0, tex_width, tex_height );

	// set the location of the sprite
	//setXYWH(&poly, 0,0, tex_width, tex_height);

    //pos.vx = x;
    //pos.vy = y;

    width = tex_width;
    height = tex_height;

	setXYWH( &poly, pos.vx, pos.vy, width, height );
	setUVWH( &poly, clip_x, clip_y, width, height );
}

void Sprite::Animate(void) {

	anim_count++;

	// switch to the next clip if necessary
	if ( anim_count >= anim_speed ) {
		anim_count = 0;

		clip_x += width;
		if ( clip_x >= tex_width ) {
			clip_x = 0; clip_y += height;

			if ( clip_y >= tex_height ) {
				clip_y = 0;
			}
		}

		setUVWH( &poly, clip_x, clip_y, width, height );
	}
}

void Sprite::Draw() {
	DrawPrim(&poly);
}

void Sprite::Translate( int x, int y )
{
    pos.vx += x;
    pos.vy += y;

    setXYWH( &poly, pos.vx, pos.vy, width, height );
	setUVWH( &poly, clip_x, clip_y, width, height );
}

void Sprite::SetPosAndSize( int x, int y, int w, int h )
{
    pos.vx = x;
    pos.vy = y;

    width = w;
    height = h;

	setXYWH( &poly, pos.vx, pos.vy, width, height );
	setUVWH( &poly, clip_x, clip_y, width, height );
}

void Sprite::SetX( int x ) {
	pos.vx = x;
	setXYWH( &poly, pos.vx, pos.vy, width, height );
	setUVWH( &poly, clip_x, clip_y, width, height );
}

void Sprite::SetY( int y ) {
	pos.vy = y;
	setXYWH( &poly, pos.vx, pos.vy, width, height );
	setUVWH( &poly, clip_x, clip_y, width, height );
}

int Sprite::GetX() {
    return pos.vx;
}

int Sprite::GetY() {
    return pos.vy;
}

// set the ACTUAL width and height of the sprite, not its
// texture size
void Sprite::SetWidth( int w ) {
	width = w;
	setXYWH( &poly, pos.vx, pos.vy, width, height );
	setUVWH( &poly, clip_x, clip_y, width, height );
}
void Sprite::SetHeight( int h ) {
	height = h;
	setXYWH( &poly, pos.vx, pos.vy, width, height );
	setUVWH( &poly, clip_x, clip_y, width, height );
}

// set the clip location within the texture
void Sprite::SetU( int u ) {
	clip_x = u;
	setUVWH( &poly, clip_x, clip_y, width, height );
}
void Sprite::SetV( int v ) {
	clip_y = v;
	setUVWH( &poly, clip_x, clip_y, width, height );
}

u_short Sprite::GetClutId() {
    return clut_id;
}
u_short Sprite::GetTextureId() {
    return tex_id;
}

void Sprite::LoadClut( u_long* clut )
{
    clut_id = ::LoadClut( clut, clut_x, clut_y );
}

