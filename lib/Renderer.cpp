#include "Renderer.h"

Renderer::Renderer()
{
    camera = NULL;
    numLights = 0;
}
Renderer::~Renderer()
{
}

void Renderer::Init()
{
    // Reset graphics
    //ResetCallback();
    ResetGraph(0);
    //PadInit(0);

    // init CD subsystem
    CdReadyCallback(0); // reset callback pointers
    CdSyncCallback(0);
    CdReadCallback(0);
    CdInit();
    CdSetDebug(0);

    // First buffer
    SetDefDispEnv( &buffers[0].disp,
                    0, 0,
                    SCREEN_WIDTH, SCREEN_HEIGHT );
    SetDefDrawEnv( &buffers[0].draw,
                    0, SCREEN_HEIGHT,
                    SCREEN_WIDTH, SCREEN_HEIGHT );

    // Second buffer
    SetDefDispEnv( &buffers[1].disp,
                    0, SCREEN_HEIGHT,
                    SCREEN_WIDTH, SCREEN_HEIGHT );
    SetDefDrawEnv( &buffers[1].draw,
                    0, 0,
                    SCREEN_WIDTH, SCREEN_HEIGHT );

    buffers[0].draw.isbg = 1;               // Enable clear
    setRGB0( &buffers[0].draw, 63, 0, 127 );  // Set clear color
    // buffers[0].draw.dtd = 1; // enable dither processing

    buffers[1].draw.isbg = 1;
    setRGB0( &buffers[1].draw, 63, 0, 127 );
    // buffers[1].draw.dtd = 1; // enable dither processing

    // apply the first drawing env
    PutDrawEnv( &buffers[0].draw );

    // Load debug bios font
    FntLoad(960, 256); // load basic font pattern
    // screen X,Y | max text length X,Y | \
    // autmatic background clear 0,1 | max characters (eg: 50).
    SetDumpFnt(FntOpen(5, 20, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 512));

    // clear order tables
    ClearOTagR( (u_long *)buffers[0].ot, OT_LEN );
    ClearOTagR( (u_long *)buffers[1].ot, OT_LEN );

    // init the gte
    InitGeom();

    // set GTE offset (recommended method of centering)
    // set to center of screen (width/2, height/2);
    SetGeomOffset( SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1 );

    // set screen depth (FOV control, width/2 works best)
    SetGeomScreen(SCREEN_WIDTH >> 1);

    // Point to the initial work buffer
    curBuffer = &buffers[0];
    nextPrim  = curBuffer->p;

    loadDefaultTexture();

    lightMatrices.color = (MATRIX){
// light source #0                #1  #2
                Light::DefaultColor.vx, 0,  0, // R
                Light::DefaultColor.vy, 0,  0, // G
                Light::DefaultColor.vz, 0,  0  // B
    };

    lightAngle = (SVECTOR){
        Light::DefaultAngle.vx, Light::DefaultAngle.vy, Light::DefaultAngle.vz, 0
    };

    lightMatrices.pos = (MATRIX){
        Light::DefaultPos.vx,   Light::DefaultPos.vy,   Light::DefaultPos.vz, // light source #0
        0,        0,        0,        //              #1
        0,        0,        0         //              #2
    };

    SetGraphDebug(0);
}

void Renderer::Update()
{
    FntFlush(-1);


    PutDispEnv( &curBuffer->disp );  // Apply the DISPENV/DRAWENVs
    PutDrawEnv( &curBuffer->draw );

    SetDispMask(1);         // Enable the display

    DrawOTag( (u_long*)curBuffer->ot+(OT_LEN-1) );

    curBuffer = (curBuffer == &buffers[0] ? &buffers[1] : &buffers[0]);
    nextPrim = curBuffer->p;         // reset the primitive ot pointer

    ClearOTagR( (u_long*)curBuffer->ot, OT_LEN );

    updateLights();

    DrawSync(0);
    VSync(0);               // Wait for vertical retrace
}

void Renderer::DrawModel(const Model& model)
{
    static MATRIX workMat;

    // The GTE has one matrix register; its best to save it
    PushMatrix();

    // Set GTE rotation and translation
    if (camera == NULL)
    {
        if ( numLights > 0 )
        {
            static MATRIX res; // resulting calculated matrix

            // TODO - setbackcolor - variable - is this ambient?
            SetBackColor( 63, 63, 63 );
            SetColorMatrix( &lightMatrices.color );

            // calculate the angle matrix in relative screen coords
            RotMatrix( &lightAngle, &lightMatrices.angle );
            MulMatrix( &lightMatrices.angle, (MATRIX*)&model.modelMat );

            // calculate the rottrans matrix for the light
            MulMatrix0( &lightMatrices.pos, &lightMatrices.angle, &res );

            // send the resulting light matrix to the GTE
            SetLightMatrix( &res );
        }
        SetRotMatrix( (MATRIX*)&model.modelMat );
        SetTransMatrix( (MATRIX*)&model.modelMat );
    }
    else
    {
        CompMatrixLV( (MATRIX*)&camera->viewMat, (MATRIX*)&model.modelMat, &workMat );
        if ( numLights > 0 )
        {
            static MATRIX res; // resulting calculated matrix

            // TODO - setbackcolor - variable - is this ambient?
            SetBackColor( 63, 63, 63 );
            SetColorMatrix( &lightMatrices.color );

            // calculate the angle matrix in relative screen coords
            RotMatrix( &lightAngle, &lightMatrices.angle );
            MulMatrix( &lightMatrices.angle, (MATRIX*)&workMat );

            // calculate the rottrans matrix for the light
            MulMatrix0( &lightMatrices.pos, &lightMatrices.angle, &res );

            // send the resulting light matrix to the GTE
            SetLightMatrix( &res );
        }
        SetRotMatrix( &workMat );
        SetTransMatrix( &workMat );
    }

    if ( model.numIndices > 0 ) {
        RenderWithIndices( model );
    } else {
        RenderWithVertices( model );
    }

    // Restore the matrix register
    PopMatrix();
}

void Renderer::DrawMd2Model( Md2Model& model, PSXMatrix& modelMat, Texture& modelTex )
{
    static MATRIX workMat;
    
    // TODO
    const u_int frameIndex = 0;

    // The GTE has one matrix register; its best to save it
    PushMatrix();

    // Set GTE rotation and translation
    if (camera == NULL)
    {
        if ( numLights > 0 )
        {
            static MATRIX res; // resulting calculated matrix

            // TODO - setbackcolor - variable - is this ambient?
            SetBackColor( 63, 63, 63 );
            SetColorMatrix( &lightMatrices.color );

            // calculate the angle matrix in relative screen coords
            RotMatrix( &lightAngle, &lightMatrices.angle );
            MulMatrix( &lightMatrices.angle, (MATRIX*)&modelMat );

            // calculate the rottrans matrix for the light
            MulMatrix0( &lightMatrices.pos, &lightMatrices.angle, &res );

            // send the resulting light matrix to the GTE
            SetLightMatrix( &res );
        }
        SetRotMatrix( (MATRIX*)&modelMat );
        SetTransMatrix( (MATRIX*)&modelMat );
    }
    else
    {
        CompMatrixLV( (MATRIX*)&camera->viewMat, (MATRIX*)&modelMat, &workMat );
        if ( numLights > 0 )
        {
            static MATRIX res; // resulting calculated matrix

            // TODO - setbackcolor - variable - is this ambient?
            SetBackColor( 63, 63, 63 );
            SetColorMatrix( &lightMatrices.color );

            // calculate the angle matrix in relative screen coords
            RotMatrix( &lightAngle, &lightMatrices.angle );
            MulMatrix( &lightMatrices.angle, (MATRIX*)&workMat );

            // calculate the rottrans matrix for the light
            MulMatrix0( &lightMatrices.pos, &lightMatrices.angle, &res );

            // send the resulting light matrix to the GTE
            SetLightMatrix( &res );
        }
        SetRotMatrix( &workMat );
        SetTransMatrix( &workMat );
    }

    //if ( model.numIndices > 0 ) {
    //    RenderWithIndices( model );
    //} else {
        RenderMd2ModelAnimated( model, modelTex );
    //}

    // Restore the matrix register
    PopMatrix();
}

void Renderer::DrawString( Font& font, int x, int y, const char* msg )
{
    size_t   i;
    //POLY_FT4 sprite;
    u_int    u = 0;
    u_int    v = 0;
    int texId;
    int clutId;
    float xPos = x;
    size_t   message_length = strlen( msg );

    POLY_FT4* sprite = (POLY_FT4*)nextPrim;
    SetPolyFT4( sprite );
    SetShadeTex( sprite, 1); // shadetex disable

    setUVWH( sprite, 0,0, (u_char)font.fontWidth, (u_char)font.fontHeight );
    setXYWH( sprite, x,y, (u_char)font.fontWidth, (u_char)font.fontHeight );

    texId = font.texture.GetTexturePageId();
    clutId = font.texture.GetClutId();

    sprite->tpage = texId;
    sprite->clut  = clutId;

    for ( i = 0; i < message_length; ++i )
    {
        xPos += font.fontWidth;

        if ( msg[i] != ' ' && (u_int)msg[i] >= 97 ) {
            font.CalcCharUV( msg[i], &u, &v );

            SetPolyFT4( sprite );
            SetShadeTex( sprite, 1 );

            sprite->tpage = texId;
            sprite->clut  = clutId;

            setUVWH( sprite, u,v, (short)font.fontWidth, (short)font.fontHeight );
            setXYWH( sprite, (int)xPos,y, (short)font.fontWidth, (short)font.fontHeight );

            //DrawPrim( &sprite );
            addPrim( &curBuffer->ot[0], sprite );
            sprite++;
        }
        else if (msg[i] >= '0' && msg[i] <= '9')
        {
            font.CalcNumberUV( msg[i] - '0', &u, &v );

            SetPolyFT4( sprite );
            SetShadeTex( sprite, 1 );

            sprite->tpage = texId;
            sprite->clut  = clutId;

            setUVWH( sprite, u,v, (short)font.fontWidth, (short)font.fontHeight );
            setXYWH( sprite, (int)xPos,y, (short)font.fontWidth, (short)font.fontHeight );

            //DrawPrim( &sprite );
            addPrim( &curBuffer->ot[0], sprite );
            sprite++;
        }
    }
    nextPrim = (char*)sprite;
}

void Renderer::RenderWithIndices( const Model& model )
{
    u_int i;
    int isomote;
    long p, otz, opz, flg;
    CVECTOR lighted_color = {127, 127, 127, 0};
    POLY_FT3* prim = (POLY_FT3*)nextPrim;

    for ( i=0; i<model.numIndices; i++ )
    {
        SetPolyFT3(prim);

        // translate from local coordinates to screen
        // coordinates using RotAverageNclip4()
        // otz represents 1/4 value of the average of z
        // value of each vertex
        isomote = RotAverageNclip3(&model.vertices[model.indices[i].v0], // SVECTOR *v0, v1, v2, v3
                                   &model.vertices[model.indices[i].v1],
                                   &model.vertices[model.indices[i].v2],

                                  (long*)&(prim->x0),
                                  (long*)&(prim->x1), // long *sxy0, sxy1
                                  (long*)&(prim->x2), // long *sxy2, sxy3

                                  &p, &otz, &flg);

        if (isomote > 0) {
            // If our depth is valid, add it to the order table
            if (otz > 0 && otz < OT_LEN) {

                if ( model.normals != NULL && numLights > 0 ) {
                    lighted_color.cd = prim->code;
                    //NormalColorCol3(&model.normals[model.indices[i].v0], // normal input
                    //                &model.normals[model.indices[i].v1],
                    //                &model.normals[model.indices[i].v2],
                    //                &lighted_color, // primary color input
                    //                (CVECTOR*)&prim->r0, // outputs
                    //                (CVECTOR*)&prim->r1,
                    //                (CVECTOR*)&prim->r2);

                    NormalColorCol( &model.normals[model.indices[i].v0], // normal input
                                   &lighted_color,  // primary color input
                                   (CVECTOR*)&prim->r0 ); // output results

                }
                else
                {
                    setRGB0(prim, 127, 127, 127);
                }


                // default texture size
                // TODO - change to use internal texture size instead of default
                //setUV3(prim,
                //       0, 0,
                //       32, 0,
                //       32, 32);
                setUV3(
                    prim,
                    model.texCoords[model.indices[i].v0*2 + 0],
                    model.texCoords[model.indices[i].v0*2 + 1],
                    model.texCoords[model.indices[i].v1*2 + 0],
                    model.texCoords[model.indices[i].v1*2 + 1],
                    model.texCoords[model.indices[i].v2*2 + 0],
                    model.texCoords[model.indices[i].v2*2 + 1]);
                prim->tpage = tpageId;
                prim->clut = 0; // TODO - support
                addPrim( curBuffer->ot+otz, prim );
            }
        }

        // increment to the next packet area
        prim++;
    }


    // update the next packet area
    nextPrim = (char*)prim;
}

void Renderer::RenderMd2Model( Md2Model& model, Texture& modelTex )
{
    // TODO
    const u_int frameIndex = 0;
    u_int i;
    int isomote;
    long p, otz, opz, flg;
    long frameTranslate[3];
    long frameScale[3];
    long md2Scale = float2fixed(model.md2Scale);
    SVECTOR xyzPos[3];
    CVECTOR lighted_color = {127, 127, 127, 0};
    POLY_FT3* prim = (POLY_FT3*)nextPrim;

    const Md2Model::Keyframe& frame = model.GetKeyframe( frameIndex );
    frameTranslate[0] = float2fixed(frame.translate[0]);
    frameTranslate[1] = float2fixed(frame.translate[1]);
    frameTranslate[2] = float2fixed(frame.translate[2]);

    frameScale[0] = float2fixed(frame.scale[0]);
    frameScale[1] = float2fixed(frame.scale[1]);
    frameScale[2] = float2fixed(frame.scale[2]);

// optimized version of float2fixed compared to * 4096 version
#define float2fixed(val) ((val) << 12)
#define fixedMul(a,b) (((a)*(b)) >> 12)

    // for each triangle
    for ( u_int t = 0; t < model.md2TriangleCount; t++ )
    {
        SetPolyFT3(prim);
        const Md2Model::Vertex& vert0 = model.GetFrameVertex( frameIndex,
                                                              model.md2Triangles[t].vertex[0] );
        const Md2Model::Vertex& vert1 = model.GetFrameVertex( frameIndex,
                                                              model.md2Triangles[t].vertex[1] );
        const Md2Model::Vertex& vert2 = model.GetFrameVertex( frameIndex, 
                                                              model.md2Triangles[t].vertex[2] );

        
        const Md2Model::TexCoord& texc0 = model.md2TexCoords[ model.md2Triangles[t].uv[0] ];
        const Md2Model::TexCoord& texc1 = model.md2TexCoords[ model.md2Triangles[t].uv[1] ];
        const Md2Model::TexCoord& texc2 = model.md2TexCoords[ model.md2Triangles[t].uv[2] ];
        
        xyzPos[0].vx = fixedMul((fixedMul(frameScale[0], float2fixed(vert0.v[0])) + frameTranslate[0]), md2Scale);
        xyzPos[0].vz = fixedMul((fixedMul(frameScale[1], float2fixed(vert0.v[1])) + frameTranslate[1]), md2Scale);
        xyzPos[0].vy = fixedMul((fixedMul(frameScale[2], float2fixed(vert0.v[2])) + frameTranslate[2]), md2Scale);
        xyzPos[1].vx = fixedMul((fixedMul(frameScale[0], float2fixed(vert1.v[0])) + frameTranslate[0]), md2Scale);
        xyzPos[1].vz = fixedMul((fixedMul(frameScale[1], float2fixed(vert1.v[1])) + frameTranslate[1]), md2Scale);
        xyzPos[1].vy = fixedMul((fixedMul(frameScale[2], float2fixed(vert1.v[2])) + frameTranslate[2]), md2Scale);
        xyzPos[2].vx = fixedMul((fixedMul(frameScale[0], float2fixed(vert2.v[0])) + frameTranslate[0]), md2Scale);
        xyzPos[2].vz = fixedMul((fixedMul(frameScale[1], float2fixed(vert2.v[1])) + frameTranslate[1]), md2Scale);
        xyzPos[2].vy = fixedMul((fixedMul(frameScale[2], float2fixed(vert2.v[2])) + frameTranslate[2]), md2Scale);

        // translate from local coordinates to screen
        // coordinates using RotAverageNclip4()
        // otz represents 1/4 value of the average of z
        // value of each vertex
        isomote = RotAverageNclip3(&xyzPos[2], // SVECTOR *v0, v1, v2, v3
                                   &xyzPos[1],
                                   &xyzPos[0],

                                  (long*)&(prim->x2),
                                  (long*)&(prim->x1), // long *sxy0, sxy1
                                  (long*)&(prim->x0), // long *sxy2, sxy3

                                  &p, &otz, &flg);

        if (isomote > 0) {
            // If our depth is valid, add it to the order table
            if (otz > 0 && otz < OT_LEN) {
                //printf("uv,uv,uv: (%d,%d), (%d,%d), (%d,%d)\n",
                //        texc0.u, texc0.v,   // vert 0
                //       texc1.u, texc1.v,   // vert 1
                //       texc2.u, texc2.v);
                setUV3( prim,
                       texc0.u, texc0.v,   // vert 0
                       texc1.u, texc1.v,   // vert 1
                       texc2.u, texc2.v); // vert 2
                prim->tpage = modelTex.tpage_id;
                prim->clut = //modelTex.clut_id
                    0;
                setRGB0( prim, 127, 127, 127);
                addPrim( curBuffer->ot+otz, prim );
            }
        }
        
        // increment to the next packet area
        prim++;
    }

#undef fixedMul
#undef float2fixed

    // update the next packet area
    nextPrim = (char*)prim;
}

void Renderer::RenderMd2ModelAnimated( Md2Model& model, Texture& modelTex )
{
    u_int i;
    int isomote;
    long p, otz, opz, flg;
    long frameTranslateA[3];
    long frameScaleA[3];
    long frameTranslateB[3];
    long frameScaleB[3];
    long md2Scale = float2fixed(model.md2Scale);
    long interp = float2fixed(model.animState.interp);
    VECTOR xyzPosA[3];
    VECTOR xyzPosB[3];
    SVECTOR xyzPos[3]; // final linearly interpolated and scaled coord
    CVECTOR lighted_color = {127, 127, 127, 0};
    POLY_FT3* prim = (POLY_FT3*)nextPrim;

    const Md2Model::Keyframe& frameA = model.GetKeyframe( model.animState.curFrame );
    const Md2Model::Keyframe& frameB = model.GetKeyframe( model.animState.nextFrame );
    
    frameTranslateA[0] = float2fixed(frameA.translate[0]);
    frameTranslateA[1] = float2fixed(frameA.translate[1]);
    frameTranslateA[2] = float2fixed(frameA.translate[2]);
    frameTranslateB[0] = float2fixed(frameB.translate[0]);
    frameTranslateB[1] = float2fixed(frameB.translate[1]);
    frameTranslateB[2] = float2fixed(frameB.translate[2]);

    frameScaleA[0] = float2fixed(frameA.scale[0]);
    frameScaleA[1] = float2fixed(frameA.scale[1]);
    frameScaleA[2] = float2fixed(frameA.scale[2]);
    frameScaleB[0] = float2fixed(frameB.scale[0]);
    frameScaleB[1] = float2fixed(frameB.scale[1]);
    frameScaleB[2] = float2fixed(frameB.scale[2]);

// optimized version of float2fixed compared to * 4096 version
#define float2fixed(val) ((val) << 12)
#define fixedMul(a,b) (((a)*(b)) >> 12)

    // for each triangle
    for ( u_int t = 0; t < model.md2TriangleCount; t++ )
    {
        SetPolyFT3(prim);
        const Md2Model::Vertex& vertA0 = model.GetFrameVertex( model.animState.curFrame,
                                                              model.md2Triangles[t].vertex[0] );
        const Md2Model::Vertex& vertA1 = model.GetFrameVertex( model.animState.curFrame,
                                                              model.md2Triangles[t].vertex[1] );
        const Md2Model::Vertex& vertA2 = model.GetFrameVertex( model.animState.curFrame, 
                                                              model.md2Triangles[t].vertex[2] );
        const Md2Model::Vertex& vertB0 = model.GetFrameVertex( model.animState.nextFrame,
                                                              model.md2Triangles[t].vertex[0] );
        const Md2Model::Vertex& vertB1 = model.GetFrameVertex( model.animState.nextFrame,
                                                              model.md2Triangles[t].vertex[1] );
        const Md2Model::Vertex& vertB2 = model.GetFrameVertex( model.animState.nextFrame, 
                                                              model.md2Triangles[t].vertex[2] );

        
        const Md2Model::TexCoord& texc0 = model.md2TexCoords[ model.md2Triangles[t].uv[0] ];
        const Md2Model::TexCoord& texc1 = model.md2TexCoords[ model.md2Triangles[t].uv[1] ];
        const Md2Model::TexCoord& texc2 = model.md2TexCoords[ model.md2Triangles[t].uv[2] ];
        
        xyzPosA[0].vx = fixedMul(frameScaleA[0], float2fixed(vertA0.v[0])) + frameTranslateA[0];
        xyzPosA[0].vz = fixedMul(frameScaleA[1], float2fixed(vertA0.v[1])) + frameTranslateA[1];
        xyzPosA[0].vy = fixedMul(frameScaleA[2], float2fixed(vertA0.v[2])) + frameTranslateA[2];
        xyzPosA[1].vx = fixedMul(frameScaleA[0], float2fixed(vertA1.v[0])) + frameTranslateA[0];
        xyzPosA[1].vz = fixedMul(frameScaleA[1], float2fixed(vertA1.v[1])) + frameTranslateA[1];
        xyzPosA[1].vy = fixedMul(frameScaleA[2], float2fixed(vertA1.v[2])) + frameTranslateA[2];
        xyzPosA[2].vx = fixedMul(frameScaleA[0], float2fixed(vertA2.v[0])) + frameTranslateA[0];
        xyzPosA[2].vz = fixedMul(frameScaleA[1], float2fixed(vertA2.v[1])) + frameTranslateA[1];
        xyzPosA[2].vy = fixedMul(frameScaleA[2], float2fixed(vertA2.v[2])) + frameTranslateA[2];

        xyzPosB[0].vx = fixedMul(frameScaleB[0], float2fixed(vertB0.v[0])) + frameTranslateB[0];
        xyzPosB[0].vz = fixedMul(frameScaleB[1], float2fixed(vertB0.v[1])) + frameTranslateB[1];
        xyzPosB[0].vy = fixedMul(frameScaleB[2], float2fixed(vertB0.v[2])) + frameTranslateB[2];
        xyzPosB[1].vx = fixedMul(frameScaleB[0], float2fixed(vertB1.v[0])) + frameTranslateB[0];
        xyzPosB[1].vz = fixedMul(frameScaleB[1], float2fixed(vertB1.v[1])) + frameTranslateB[1];
        xyzPosB[1].vy = fixedMul(frameScaleB[2], float2fixed(vertB1.v[2])) + frameTranslateB[2];
        xyzPosB[2].vx = fixedMul(frameScaleB[0], float2fixed(vertB2.v[0])) + frameTranslateB[0];
        xyzPosB[2].vz = fixedMul(frameScaleB[1], float2fixed(vertB2.v[1])) + frameTranslateB[1];
        xyzPosB[2].vy = fixedMul(frameScaleB[2], float2fixed(vertB2.v[2])) + frameTranslateB[2];


        shortLerpScale( xyzPos[0], xyzPosA[0], xyzPosB[0], interp, md2Scale );
        shortLerpScale( xyzPos[1], xyzPosA[1], xyzPosB[1], interp, md2Scale );
        shortLerpScale( xyzPos[2], xyzPosA[2], xyzPosB[2], interp, md2Scale );

        // translate from local coordinates to screen
        // coordinates using RotAverageNclip4()
        // otz represents 1/4 value of the average of z
        // value of each vertex
        isomote = RotAverageNclip3(&xyzPos[2], // SVECTOR *v0, v1, v2, v3
                                   &xyzPos[1],
                                   &xyzPos[0],

                                  (long*)&(prim->x2),
                                  (long*)&(prim->x1), // long *sxy0, sxy1
                                  (long*)&(prim->x0), // long *sxy2, sxy3

                                  &p, &otz, &flg);

        if (isomote > 0) {
            // If our depth is valid, add it to the order table
            if (otz > 0 && otz < OT_LEN) {
                //printf("uv,uv,uv: (%d,%d), (%d,%d), (%d,%d)\n",
                //        texc0.u, texc0.v,   // vert 0
                //       texc1.u, texc1.v,   // vert 1
                //       texc2.u, texc2.v);
                setUV3( prim,
                       texc0.u, texc0.v,   // vert 0
                       texc1.u, texc1.v,   // vert 1
                       texc2.u, texc2.v); // vert 2
                prim->tpage = modelTex.tpage_id;
                prim->clut = /*modelTex.clut_id*/0;
                setRGB0( prim, 127, 127, 127);
                addPrim( curBuffer->ot+otz, prim );
            }
        }
        
        // increment to the next packet area
        prim++;
    }

#undef fixedMul
#undef float2fixed

    // update the next packet area
    nextPrim = (char*)prim;
}

void Renderer::RenderWithVertices( const Model& model )
{
    u_int i;
    int isomote;
    long p, otz, opz, flg;
    CVECTOR lighted_color = {127, 127, 127, 0};
    POLY_FT4* prim = (POLY_FT4*)nextPrim;

    for ( i=0; i<model.numVertices; i+=4 )
    {
        SetPolyFT4(prim);

        // translate from local coordinates to screen
        // coordinates using RotAverageNclip4()
        // otz represents 1/4 value of the average of z
        // value of each vertex
        isomote = RotAverageNclip4(&model.vertices[i+0], // SVECTOR *v0, v1, v2, v3
                                   &model.vertices[i+1],
                                   &model.vertices[i+2],
                                   &model.vertices[i+3],

                                  (long*)&(prim->x0),
                                  (long*)&(prim->x1), // long *sxy0, sxy1
                                  (long*)&(prim->x3), // three then two intentionally
                                                           // the vertices order must be screwed up
                                  (long*)&(prim->x2), // long *sxy2, sxy3

                                  &p, &otz, &flg);

        if (isomote > 0) {
            // If our depth is valid, add it to the order table
            if (otz > 0 && otz < OT_LEN) {

                if ( model.normals != NULL && numLights > 0 )
                {
                    lighted_color.cd = prim->code;
                    NormalColorCol( &model.normals[i], // normal input
                                   &lighted_color,  // primary color input
                                   (CVECTOR*)&prim->r0 ); // output results
                    // for POLY_GT3
                    //NormalColorCol3(&model.normals[i+0], // normal input
                    //                &model.normals[i+1],
                    //                &model.normals[i+2],
                    //                &lighted_color, // primary color input
                    //                (CVECTOR*)&prim->r0, // outputs
                    //                (CVECTOR*)&prim->r1,
                    //                (CVECTOR*)&prim->r2);
                }
                else
                {
                    setRGB0(prim, 127, 127, 127);
                }
                setUV3(prim,
                       0, 0,
                       32, 0,
                       32, 32);
                prim->tpage = tpageId;
                prim->clut = 0;
                addPrim( curBuffer->ot+otz, prim );
            }
        }

        // increment to the next packet area
        prim++;
    }

    // update the next packet area
    nextPrim = (char*)prim;
}

void Renderer::updateLights()
{
    int i;
    for ( i = 0; i < numLights; i++ )
    {
        int colorCol = i;
        int posRow = i;

        lightMatrices.color.m[0][colorCol] = lights[i]->color.vx;
        lightMatrices.color.m[1][colorCol] = lights[i]->color.vy;
        lightMatrices.color.m[2][colorCol] = lights[i]->color.vz;

        lightMatrices.pos.m[posRow][0] = lights[i]->pos.vx;
        lightMatrices.pos.m[posRow][1] = lights[i]->pos.vy;
        lightMatrices.pos.m[posRow][2] = lights[i]->pos.vz;

        // TODO - figure out light angle
        setVector( &lightAngle, lights[i]->angle.vx,
                                lights[i]->angle.vy,
                                lights[i]->angle.vz );
    }
}

