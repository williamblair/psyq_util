
// ================================================================================================
// -*- C++ -*-
// File: matrix.hpp
// Author: Guilherme R. Lampert
// Created on: 11/03/15
// Brief: Homogeneous 4x4 matrix, used for all kinds of object transforms.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2015 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#ifndef PSXMATH_MATRIX_HPP
#define PSXMATH_MATRIX_HPP

#include <PSXVector.h>
#include <string.h> // For `memcpy()`

#define fixedMul(a,b) (((a)*(b))>>12)
#define fixedDiv(a,b) ((long)((((long long)(a))<<12) / ((long long)(b))))

static inline long float2fixed(const float a)
{
    return (long)(a * 4096);
}

// ========================================================
// struct PSXMatrix:
// ========================================================

struct PSXMatrix
{
	PSXMatrix() { } // Uninitialized
	explicit PSXMatrix(const long data[][4]);
	PSXMatrix(long m11, long m12, long m13, long m14,
	       long m21, long m22, long m23, long m24,
	       long m31, long m32, long m33, long m34);

	PSXMatrix(const PSXMatrix & other);
	PSXMatrix & operator = (const PSXMatrix & other);

	void makeIdentity();

	void makeTranslation(long x, long y, long z);
	void makeTranslation(const PSXVector & v);

	void makeScaling(long x, long y, long z);
	void makeScaling(const PSXVector & v);

	void makeRotationX(long radians);
	void makeRotationY(long radians);
	void makeRotationZ(long radians);

	void makeLookAt(const PSXVector & vFrom, const PSXVector & vTo, const PSXVector & vUp);
	void makePerspectiveProjection(long fovy, long aspect, long scrW, long scrH,
	                               long zNear, long zFar, long projScale = float2fixed(4096.0f));

	//short& operator() (unsigned int row, unsigned int column)       { return (column > 2) ? t[row] : m[row][column]; }
	short  operator() (unsigned int row, unsigned int column) const { return (column > 2) ? t[row] : m[row][column]; }

    // the same as in struct MATRIX in libgte.h
    short m[3][3];  // 3x3 rotation matrix
    long  t[3];     // transfer vector (translation)
};

// ========================================================
// PSXMatrix inline methods and operators:
// ========================================================

inline PSXMatrix::PSXMatrix(const long m11, const long m12, const long m13, const long m14,
                      const long m21, const long m22, const long m23, const long m24,
                      const long m31, const long m32, const long m33, const long m34)
{
	m[0][0] = m11;  m[0][1] = m12;  m[0][2] = m13;  t[0] = m14;
	m[1][0] = m21;  m[1][1] = m22;  m[1][2] = m23;  t[1] = m24;
	m[2][0] = m31;  m[2][1] = m32;  m[2][2] = m33;  t[2] = m34;
}

inline PSXMatrix::PSXMatrix(const long data[][4])
{
	// Input is not necessarily aligned, so default to a memcpy.
	//memcpy(elem, data, sizeof(long) * 16);
	m[0][0] = data[0][0];  m[0][1] = data[0][1];  m[0][2] = data[0][2];  t[0] = data[0][3];
	m[1][0] = data[1][0];  m[1][1] = data[1][1];  m[1][2] = data[1][2];  t[1] = data[1][3];
	m[2][0] = data[2][0];  m[2][1] = data[2][1];  m[2][2] = data[2][2];  t[2] = data[2][3];
}

inline PSXMatrix::PSXMatrix(const PSXMatrix & other)
{
#if 0

	asm volatile (
		"lq $6, 0x00(%1) \n\t"
		"lq $7, 0x10(%1) \n\t"
		"lq $8, 0x20(%1) \n\t"
		"lq $9, 0x30(%1) \n\t"
		"sq $6, 0x00(%0) \n\t"
		"sq $7, 0x10(%0) \n\t"
		"sq $8, 0x20(%0) \n\t"
		"sq $9, 0x30(%0) \n\t"
		: : "r" (elem), "r" (other.elem)
		: "$6", "$7", "$8", "$9"
	);

#else

	memcpy(m, other.m, sizeof(m));
    memcpy(t, other.t, sizeof(t));

#endif
}

inline PSXMatrix & PSXMatrix::operator = (const PSXMatrix & other)
{
#if 0

	asm volatile (
		"lq $6, 0x00(%1) \n\t"
		"lq $7, 0x10(%1) \n\t"
		"lq $8, 0x20(%1) \n\t"
		"lq $9, 0x30(%1) \n\t"
		"sq $6, 0x00(%0) \n\t"
		"sq $7, 0x10(%0) \n\t"
		"sq $8, 0x20(%0) \n\t"
		"sq $9, 0x30(%0) \n\t"
		: : "r" (elem), "r" (other.elem)
		: "$6", "$7", "$8", "$9"
	);
	return *this;

#else

	memcpy(m, other.m, sizeof(m));
    memcpy(t, other.t, sizeof(t));
	return *this;

#endif
}

inline void PSXMatrix::makeIdentity()
{
#if 0

	asm volatile (
		"vsub.xyzw  vf4, vf0, vf0 \n\t"
		"vadd.w     vf4, vf4, vf0 \n\t"
		"vmr32.xyzw vf5, vf4      \n\t"
		"vmr32.xyzw vf6, vf5      \n\t"
		"vmr32.xyzw vf7, vf6      \n\t"
		"sqc2       vf4, 0x30(%0) \n\t"
		"sqc2       vf5, 0x20(%0) \n\t"
		"sqc2       vf6, 0x10(%0) \n\t"
		"sqc2       vf7, 0x0(%0)  \n\t"
		: : "r" (elem)
	);

#elif 0

	elem[0][0] = 1.0f;  elem[0][1] = 0.0f;  elem[0][2] = 0.0f;  elem[0][3] = 0.0f;
	elem[1][0] = 0.0f;  elem[1][1] = 1.0f;  elem[1][2] = 0.0f;  elem[1][3] = 0.0f;
	elem[2][0] = 0.0f;  elem[2][1] = 0.0f;  elem[2][2] = 1.0f;  elem[2][3] = 0.0f;
	elem[3][0] = 0.0f;  elem[3][1] = 0.0f;  elem[3][2] = 0.0f;  elem[3][3] = 1.0f;
#else
	m[0][0] = ONE;  m[0][1] =   0;  m[0][2] =   0;
	m[1][0] =   0;  m[1][1] = ONE;  m[1][2] =   0;
	m[2][0] =   0;  m[2][1] =   0;  m[2][2] = ONE;
    t[0] = t[1] = t[2] = 0;
#endif
}

inline void PSXMatrix::makeTranslation(const long x, const long y, const long z)
{
	makeIdentity();
#if 0
	elem[3][0] = x;
	           elem[3][1] = y;
	elem[3][2] = z;
#else
    t[0] = x;
    t[1] = y;
    t[2] = z;
#endif
}

inline void PSXMatrix::makeTranslation(const PSXVector & v)
{
	makeIdentity();
#if 0
	elem[3][0] = v.x;
	elem[3][1] = v.y;
	elem[3][2] = v.z;
#else
    t[0] = v.x;
    t[1] = v.y;
    t[2] = v.z;
#endif
}

inline void PSXMatrix::makeScaling(const long x, const long y, const long z)
{
	makeIdentity();
	m[0][0] = x;
	m[1][1] = y;
	m[2][2] = z;
}

inline void PSXMatrix::makeScaling(const PSXVector & v)
{
	makeIdentity();
	m[0][0] = v.x;
	m[1][1] = v.y;
	m[2][2] = v.z;
}

inline void PSXMatrix::makeRotationX(const long radians)
{
	makeIdentity();
#if 0
	const long c = ps2math::cos(radians);
	const long s = ps2math::sin(radians);
	elem[1][1] =  c;
	elem[1][2] =  s;
	elem[2][1] = -s;
	elem[2][2] =  c;
#else
    SVECTOR svec;
    setVector(&svec, radians, 0, 0);
    // TODO - test this; according to docs RotMatrix_gte is ~2x faster
    //RotMatrix_gte(&svec, (MATRIX*)this)
    RotMatrix(&svec, (MATRIX*)this);
#endif
}

inline void PSXMatrix::makeRotationY(const long radians)
{
	makeIdentity();
#if 0
	const long c = ps2math::cos(radians);
	const long s = ps2math::sin(radians);
	elem[0][0] =  c;
	elem[2][0] =  s;
	elem[0][2] = -s;
	elem[2][2] =  c;
#else
    SVECTOR svec;
    setVector(&svec, 0, radians, 0);
    //RotMatrix_gte(&svec, (MATRIX*)this)
    RotMatrix(&svec, (MATRIX*)this);
#endif
}

inline void PSXMatrix::makeRotationZ(const long radians)
{
	makeIdentity();
#if 0
	const long c = rcos(radians);
	const long s = rsin(radians);
	elem[0][0] =  c;
	elem[0][1] =  s;
	elem[1][0] = -s;
	elem[1][1] =  c;
#else
    SVECTOR svec;
    setVector(&svec, 0, 0, radians);
    //RotMatrix_gte(&svec, (MATRIX*)this)
    RotMatrix(&svec, (MATRIX*)this);
#endif
}

inline void PSXMatrix::makeLookAt(const PSXVector & vFrom, const PSXVector & vTo, const PSXVector & vUp)
{
	const PSXVector vZ = normalize(vFrom - vTo);
	const PSXVector vX = normalize(vUp.cross(vZ));
	const PSXVector vY = vZ.cross(vX);

	m[0][0] = vX.x;  m[1][0] = vY.x;  m[2][0] = vZ.x;
	m[0][1] = vX.y;  m[1][1] = vY.y;  m[2][1] = vZ.y;
	m[0][2] = vX.z;  m[1][2] = vY.z;  m[2][2] = vZ.z;

	t[0] = -vX.dot3(vFrom);
	t[1] = -vY.dot3(vFrom);
	t[2] = -vZ.dot3(vFrom);
}

inline void PSXMatrix::makePerspectiveProjection(const long fovy, const long aspect, const long scrW, const long scrH,
                                              const long zNear, const long zFar, const long projScale)
{
	const long fovYdiv2 = fixedMul(fovy, float2fixed(0.5f));
	const long cotFOV   = fixedDiv(float2fixed(1.0f), fixedDiv(rsin(fovYdiv2), rcos(fovYdiv2)));
	const long w = fixedDiv(fixedMul(cotFOV, fixedDiv(scrW, projScale)), aspect);
	const long h = fixedMul(cotFOV, fixedDiv(scrH, projScale));

    // TODO - is this used?
	//(*this) = PSXMatrix(
	//	w,    0, 0, 0,
	//	0, -h,   0, 0,
	//	0, 0, (zFar + zNear) / (zFar - zNear),       -ONE,
	//	0, 0, fixedDiv((fixedMul(ONE,float2fixed(2.0f)) * zFar * zNear), (zFar - zNear)), 0);
	(*this) = PSXMatrix(
		w,    0, 0, 0,
		0, -h,   0, 0,
		0, 0, (zFar + zNear) / (zFar - zNear),-ONE);
}

inline PSXMatrix operator - (const PSXMatrix & M)
{
	return PSXMatrix(-M(0,0), -M(0,1), -M(0,2), -M(0,3),
	              -M(1,0), -M(1,1), -M(1,2), -M(1,3),
	              -M(2,0), -M(2,1), -M(2,2), -M(2,3));
}

inline PSXMatrix operator - (const PSXMatrix & M1, const PSXMatrix & M2)
{
	return PSXMatrix(M1(0,0) - M2(0,0), M1(0,1) - M2(0,1), M1(0,2) - M2(0,2), M1(0,3) - M2(0,3),
	              M1(1,0) - M2(1,0), M1(1,1) - M2(1,1), M1(1,2) - M2(1,2), M1(1,3) - M2(1,3),
	              M1(2,0) - M2(2,0), M1(2,1) - M2(2,1), M1(2,2) - M2(2,2), M1(2,3) - M2(2,3));
}

inline PSXMatrix operator + (const PSXMatrix & M1, const PSXMatrix & M2)
{
	return PSXMatrix(M1(0,0) + M2(0,0), M1(0,1) + M2(0,1), M1(0,2) + M2(0,2), M1(0,3) + M2(0,3),
	              M1(1,0) + M2(1,0), M1(1,1) + M2(1,1), M1(1,2) + M2(1,2), M1(1,3) + M2(1,3),
	              M1(2,0) + M2(2,0), M1(2,1) + M2(2,1), M1(2,2) + M2(2,2), M1(2,3) + M2(2,3));
}

inline PSXMatrix operator * (const PSXMatrix & M, const long s)
{
    #define sval(val) fixedMul((val), s)
	return PSXMatrix(sval(M(0,0)), sval(M(0,1)), sval(M(0,2)), sval(M(0,3)),
	              sval(M(1,0)), sval(M(1,1)), sval(M(1,2)), sval(M(1,3)),
	              sval(M(2,0)), sval(M(2,1)), sval(M(2,2)), sval(M(2,3)));
    #undef sval
}

inline PSXMatrix operator * (const long s, const PSXMatrix & M)
{
    #define sval(val) fixedMul((val), s)
	return PSXMatrix(sval(M(0,0)), sval(M(0,1)), sval(M(0,2)), sval(M(0,3)),
	              sval(M(1,0)), sval(M(1,1)), sval(M(1,2)), sval(M(1,3)),
	              sval(M(2,0)), sval(M(2,1)), sval(M(2,2)), sval(M(2,3)));
    #undef sval
}

inline PSXMatrix operator * (const PSXMatrix & M1, const PSXMatrix & M2) // PSXMatrix multiply
{
#if 0

	PSXMatrix result;
	asm volatile (
		"lqc2         vf1, 0x00(%1) \n\t"
		"lqc2         vf2, 0x10(%1) \n\t"
		"lqc2         vf3, 0x20(%1) \n\t"
		"lqc2         vf4, 0x30(%1) \n\t"
		"lqc2         vf5, 0x00(%2) \n\t"
		"lqc2         vf6, 0x10(%2) \n\t"
		"lqc2         vf7, 0x20(%2) \n\t"
		"lqc2         vf8, 0x30(%2) \n\t"
		"vmulax.xyzw  ACC, vf5, vf1 \n\t"
		"vmadday.xyzw ACC, vf6, vf1 \n\t"
		"vmaddaz.xyzw ACC, vf7, vf1 \n\t"
		"vmaddw.xyzw  vf1, vf8, vf1 \n\t"
		"vmulax.xyzw  ACC, vf5, vf2 \n\t"
		"vmadday.xyzw ACC, vf6, vf2 \n\t"
		"vmaddaz.xyzw ACC, vf7, vf2 \n\t"
		"vmaddw.xyzw  vf2, vf8, vf2 \n\t"
		"vmulax.xyzw  ACC, vf5, vf3 \n\t"
		"vmadday.xyzw ACC, vf6, vf3 \n\t"
		"vmaddaz.xyzw ACC, vf7, vf3 \n\t"
		"vmaddw.xyzw  vf3, vf8, vf3 \n\t"
		"vmulax.xyzw  ACC, vf5, vf4 \n\t"
		"vmadday.xyzw ACC, vf6, vf4 \n\t"
		"vmaddaz.xyzw ACC, vf7, vf4 \n\t"
		"vmaddw.xyzw  vf4, vf8, vf4 \n\t"
		"sqc2         vf1, 0x00(%0) \n\t"
		"sqc2         vf2, 0x10(%0) \n\t"
		"sqc2         vf3, 0x20(%0) \n\t"
		"sqc2         vf4, 0x30(%0) \n\t"
		: : "r" (&result), "r" (&M1), "r" (&M2)
	);
	return result;

#elif 0

	PSXMatrix result;
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			register long value = 0;
			for (int k = 0; k < 4; ++k)
			{
				value += M1(i,k) * M2(k,j);
			}
			result(i,j) = value;
		}
	}
	return result;

#else

    // destroys the constant rotation matrix
    PSXMatrix result;
    MulMatrix0((MATRIX*)&M1, (MATRIX*)&M2, (MATRIX*)&result);
    return result;
#endif
}

inline PSXVector operator * (const PSXMatrix & M, const PSXVector & V) // Transform point
{
#if 0

	PSXVector result;
	asm volatile (
		"lqc2         vf4, 0x0(%1)  \n\t"
		"lqc2         vf5, 0x10(%1) \n\t"
		"lqc2         vf6, 0x20(%1) \n\t"
		"lqc2         vf7, 0x30(%1) \n\t"
		"lqc2         vf8, 0x0(%2)  \n\t"
		"vmulax.xyzw  ACC, vf4, vf8 \n\t"
		"vmadday.xyzw ACC, vf5, vf8 \n\t"
		"vmaddaz.xyzw ACC, vf6, vf8 \n\t"
		"vmaddw.xyzw  vf9, vf7, vf8 \n\t"
		"sqc2         vf9, 0x0(%0)  \n\t"
		: : "r" (&result), "r" (&M), "r" (&V)
	);
	return result;

#elif 0

    PSXVector result;
	result.x = M(0,0) * V.x + M(1,0) * V.y + M(2,0) * V.z + M(3,0) * V.w;
	result.y = M(0,1) * V.x + M(1,1) * V.y + M(2,1) * V.z + M(3,1) * V.w;
	result.z = M(0,2) * V.x + M(1,2) * V.y + M(2,2) * V.z + M(3,2) * V.w;
	result.w = M(0,3) * V.x + M(1,3) * V.y + M(2,3) * V.z + M(3,3) * V.w;
    return result;

#else
    // Destroys the constant rotation matrix
    PSXVector result;
    ApplyMatrixLV((MATRIX*)&M, (VECTOR*)&V, (VECTOR*)&result);
    result.x += M.t[0];
    result.y += M.t[1];
    result.z += M.t[2];
    result.w = ONE;
    return result;
#endif
}

inline PSXMatrix transpose(const PSXMatrix & M)
{
	PSXMatrix result;
#if 0
	for (int j = 0; j < 4; ++j)
	{
		for (int i = 0; i < 4; ++i)
		{
			result(i,j) = M(j,i);
		}
	}
#endif
    // TODO - does the translate portion need to be changed also?
    for (int j = 0; j < 3; j++)
    {
        for (int i = 0; i < 3; i++)
        {
            result.m[i][j] = M.m[j][i];
        }
    }
    result.t[0] = M.t[0];
    result.t[1] = M.t[1];
    result.t[2] = M.t[2];
	return result;
}

inline short* toShortPtr(PSXMatrix & m)
{
	return (short*)(&m);
}

inline const short* toShortPtr(const PSXMatrix & m)
{
	return (const short*)(&m);
}

#undef fixedMul
#undef fixedDiv

#endif // PSXMATH_MATRIX_HPP

