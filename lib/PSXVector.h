
// ================================================================================================
// -*- C++ -*-
// File: vector.hpp
// Author: Guilherme R. Lampert
// Created on: 11/03/15
// Brief: Generic Euclidean vector type, also utilized to represent 3D points.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2015 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#ifndef PSXMATH_VECTOR_HPP
#define PSXMATH_VECTOR_HPP

#include <libgte.h>

//#include <PS2MathFuncs.h>
//#define ONE 4096
#define fixedMul(a,b) (((a)*(b))>>12)
#define fixedDiv(a,b) ((long)((((long long)(a))<<12) / ((long long)(b))))

// ========================================================
// struct PSXVector:
// ========================================================

struct PSXVector
{
	PSXVector() { } // Uninitialized
	explicit PSXVector(long v); // Replicate to xyzw
	explicit PSXVector(const long v[4]);
	PSXVector(long xx, long yy, long zz, long ww);

	PSXVector & operator += (const PSXVector & rhs);
	PSXVector & operator -= (const PSXVector & rhs);

	PSXVector & operator *= (long s);
	PSXVector & operator /= (long s);

	bool operator == (const PSXVector & rhs) const;
    PSXVector& operator = (const PSXVector& rhs);

	PSXVector cross(const PSXVector & rhs) const;
	long dot3(const PSXVector & rhs) const;
	long dot4(const PSXVector & rhs) const;

	PSXVector normalized() const;
	void normalizeSelf();
	void clampLength(long minLength, long maxLength);

	long length() const;
	long lengthSqr() const;

    // equivalent to libgte VECTOR structure
	long x;
	long y;
	long z;
	long w;
};

// ========================================================
// Global PSXVector operators and helpers:
// ========================================================

inline PSXVector operator - (const PSXVector & v1)
{
	return PSXVector(-v1.x, -v1.y, -v1.z, ONE);
}

inline PSXVector operator + (const PSXVector & v1, const PSXVector & v2)
{
	return PSXVector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, ONE);
}

inline PSXVector operator - (const PSXVector & v1, const PSXVector & v2)
{
	return PSXVector(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, ONE);
}

inline PSXVector operator * (const PSXVector & v, const long s)
{
	return PSXVector( fixedMul( v.x, s ), 
                      fixedMul( v.y, s ), 
                      fixedMul( v.z, s ), 
                      ONE );
}

inline PSXVector operator * (const long s, const PSXVector & v)
{
	return PSXVector( fixedMul( v.x, s ), 
                      fixedMul( v.y, s ), 
                      fixedMul( v.z, s ),
                      ONE );
}

inline PSXVector operator / (const PSXVector & v, const long s)
{
	return PSXVector( fixedDiv( v.x, s ), 
                      fixedDiv( v.y, s ), 
                      fixedDiv( v.z, s ), 
                      ONE );
}

inline PSXVector crossProduct(const PSXVector & v1, const PSXVector & v2)
{
    // ((a*b)/4096) - ((c*d)/4096) == (a*b - c*d)/4096
	return PSXVector( (v1.y * v2.z - v1.z * v2.y) >> 12,
                      (v1.z * v2.x - v1.x * v2.z) >> 12, 
                      (v1.x * v2.y - v1.y * v2.x) >> 12, 
                      ONE );
}

inline long dotProduct4(const PSXVector & v1, const PSXVector & v2)
{
	return ( (v1.x * v2.x + 
              v1.y * v2.y + 
              v1.z * v2.z + 
              v1.w * v2.w) >> 12);
}

inline long dotProduct3(const PSXVector & v1, const PSXVector & v2)
{
	return ( (v1.x * v2.x + 
              v1.y * v2.y + 
              v1.z * v2.z) >> 12);
}

inline PSXVector normalize(const PSXVector & v)
{
	return (v / v.length());
}

inline long * toLongPtr(PSXVector & v)
{
	return (long *)(&v);
}

inline const long * toLongPtr(const PSXVector & v)
{
	return (const long *)(&v);
}

inline PSXVector min3PerElement(const PSXVector & vec0, const PSXVector & vec1)
{
    #define min(x,y) (((x) < (y)) ? (x) : (y))
	return PSXVector(
		min(vec0.x, vec1.x),
		min(vec0.y, vec1.y),
		min(vec0.z, vec1.z), ONE);
    #undef min
}

inline PSXVector max3PerElement(const PSXVector & vec0, const PSXVector & vec1)
{
    #define max(x,y) (((x) > (y)) ? (x) : (y))
	return PSXVector(
		max(vec0.x, vec1.x),
		max(vec0.y, vec1.y),
		max(vec0.z, vec1.z), ONE);
    #undef max
}

inline void lerp(PSXVector & v0, const PSXVector & v1, const PSXVector & v2, const long t)
{
#if 0

	asm volatile (
		"lqc2      vf4, 0x0(%1)  \n\t" // vf4 = v1
		"lqc2      vf5, 0x0(%2)  \n\t" // vf5 = v2
		"mfc1      $8,  %3       \n\t" // vf6 = t
		"qmtc2     $8,  vf6      \n\t" // lerp:
		"vsub.xyz  vf7, vf5, vf4 \n\t" // vf7 = v2 - v1
		"vmulx.xyz vf8, vf7, vf6 \n\t" // vf8 = vf7 * t
		"vadd.xyz  vf9, vf8, vf4 \n\t" // vf9 = vf8 + vf4
		"sqc2      vf9, 0x0(%0)  \n\t" // v0  = vf9
		: : "r" (&v0), "r" (&v1), "r" (&v2), "f" (t)
		: "$8"
	);

#else

	v0.x = v1.x + fixedMul(t, (v2.x - v1.x));
	v0.y = v1.y + fixedMul(t, (v2.y - v1.y));
	v0.z = v1.z + fixedMul(t, (v2.z - v1.z));
	// v0.w is undefined!

#endif
}

inline void lerpScale(PSXVector & v0, const PSXVector & v1, const PSXVector & v2, const long t, const long s)
{
#if 0

	asm volatile (
		"mfc1      $8,  %3       \n\t"
		"mfc1      $9,  %4       \n\t"
		"lqc2      vf4, 0x0(%1)  \n\t" // vf4 = v1
		"lqc2      vf5, 0x0(%2)  \n\t" // vf5 = v2
		"qmtc2     $8,  vf6      \n\t" // vf6 = t
		"qmtc2     $9,  vf7      \n\t" // vf7 = s
		"vsub.xyz  vf8, vf5, vf4 \n\t" // vf8 = v2 - v1
		"vmulx.xyz vf8, vf8, vf6 \n\t" // vf8 = vf8 * t
		"vadd.xyz  vf9, vf8, vf4 \n\t" // vf9 = vf8 + vf4
		"vmulx.xyz vf9, vf9, vf7 \n\t" // vf9 = vf9 * s
		"sqc2      vf9, 0x0(%0)  \n\t" // v0  = vf9
		: : "r" (&v0), "r" (&v1), "r" (&v2), "f" (t), "f" (s)
		: "$8", "$9"
	);

#else

	v0.x = v1.x + fixedMul(t, (v2.x - v1.x));
	v0.y = v1.y + fixedMul(t, (v2.y - v1.y));
	v0.z = v1.z + fixedMul(t, (v2.z - v1.z));
	v0.x  = fixedMul(v0.x, s);
	v0.y  = fixedMul(v0.y, s);
	v0.z  = fixedMul(v0.z, s);
	// v0.w is undefined!

#endif
}

inline long distanceSqr(const PSXVector & a, const PSXVector & b)
{
#if 0

	register long dist;
	asm volatile (
		"lqc2     vf4, 0x0(%1)  \n\t" // vf4 = a
		"lqc2     vf5, 0x0(%2)  \n\t" // vf5 = b
		"vsub.xyz vf6, vf4, vf5 \n\t" // vf6 = vf4(a) - vf5(b)
		"vmul.xyz vf7, vf6, vf6 \n\t" // vf7 = vf6 * vf6
		"vaddy.x  vf7, vf7, vf7 \n\t" // dot(vf7, vf7)
		"vaddz.x  vf7, vf7, vf7 \n\t"
		"qmfc2    $2,  vf7      \n\t" // Store result on `dist`
		"mtc1     $2,  %0       \n\t"
		: "=f" (dist)
		: "r" (&a), "r" (&b)
		: "$2"
	);
	return dist;

#else

	return (a - b).lengthSqr();

#endif
}

inline PSXVector lerp(const PSXVector & v1, const PSXVector & v2, const long t)
{
	PSXVector v0;
	lerp(v0, v1, v2, t); // Forward to the optimized ASM routine
	v0.w = ONE;         // This one ensures w=1
	return v0;
}

inline void rotateAroundAxis(PSXVector & result, const PSXVector & vec, const PSXVector & axis, const long radians)
{
	// Rotate `vec` around an arbitrary `axis` by an angle in radians.
	//
	const long sinAng = rsin(radians);
	const long cosAng = rcos(radians);
	const long oneMinusCosAng = (ONE - cosAng);
	const long aX = axis.x;
	const long aY = axis.y;
	const long aZ = axis.z;

	// Calculate X component:
	long xxx = ((fixedMul( fixedMul(aX, aX), oneMinusCosAng) + cosAng)      * vec.x +
				(fixedMul( fixedMul(aX, aY), oneMinusCosAng) + fixedMul(aZ, sinAng)) * vec.y +
				(fixedMul( fixedMul(aX, aZ), oneMinusCosAng) - fixedMul(aY, sinAng)) * vec.z) >> 12;

	// Calculate Y component:
	long yyy = ((fixedMul( fixedMul(aX, aY), oneMinusCosAng) - fixedMul(aZ, sinAng)) * vec.x +
				(fixedMul( fixedMul(aY, aY), oneMinusCosAng) + cosAng)      * vec.y +
				(fixedMul( fixedMul(aY, aZ), oneMinusCosAng) + fixedMul(aX, sinAng)) * vec.z) >> 12;

	// Calculate Z component:
	long zzz = ((fixedMul( fixedMul(aX, aZ), oneMinusCosAng) + fixedMul(aY, sinAng)) * vec.x +
				(fixedMul( fixedMul(aY, aZ), oneMinusCosAng) - fixedMul(aX, sinAng)) * vec.y +
				(fixedMul( fixedMul(aZ, aZ), oneMinusCosAng) + cosAng)      * vec.z) >> 12;

	result.x = xxx;
	result.y = yyy;
	result.z = zzz;
	result.w = ONE;
}

// ========================================================
// PSXVector inline methods and operators:
// ========================================================

inline PSXVector::PSXVector(const long v)
	: x(v), y(v), z(v), w(v)
{
}

inline PSXVector::PSXVector(const long xx, const long yy, const long zz, const long ww)
	: x(xx), y(yy), z(zz), w(ww)
{
}

inline PSXVector::PSXVector(const long v[4])
	: x(v[0]), y(v[1]), z(v[2]), w(v[3])
{
}

inline PSXVector & PSXVector::operator += (const PSXVector & rhs)
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	w  = ONE;
	return *this;
}

inline PSXVector & PSXVector::operator -= (const PSXVector & rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	w  = ONE;
	return *this;
}

inline long PSXVector::dot3(const PSXVector & rhs) const
{
	return (x * rhs.x + y * rhs.y + z * rhs.z) >> 12;
}

inline long PSXVector::dot4(const PSXVector & rhs) const
{
	return (x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w) >> 12;
}

inline long PSXVector::length() const
{
	return csqrt((x * x + y * y + z * z) >> 12);
}

inline long PSXVector::lengthSqr() const
{
	return (x * x + y * y + z * z) >> 12;
}

inline PSXVector PSXVector::cross(const PSXVector & rhs) const
{
	return PSXVector( (y * rhs.z - z * rhs.y) >> 12, 
                      (z * rhs.x - x * rhs.z) >> 12, 
                      (x * rhs.y - y * rhs.x) >> 12, 
                      ONE );
}

inline PSXVector PSXVector::normalized() const
{
	return (*this) / length();
}

inline void PSXVector::normalizeSelf()
{
	//(*this) /= length();
    VectorNormal((VECTOR*)this, (VECTOR*)this);
}

inline void PSXVector::clampLength(const long minLength, const long maxLength)
{
	const long length2 = lengthSqr();
	if (length2 > (fixedMul(maxLength, maxLength)))
	{
		const long invLen = fixedMul(maxLength, fixedDiv(ONE,csqrt(length2)));
		(*this) *= invLen;
		return;
	}
	if (length2 < (fixedMul(minLength, minLength)))
	{
		const long invLen = fixedMul(minLength, fixedDiv(ONE,csqrt(length2)));
		(*this) *= invLen;
		return;
	}
}

inline PSXVector & PSXVector::operator *= (const long s)
{
	x = fixedMul(x,s);
	y = fixedMul(y,s);
	z = fixedMul(z,s);
	w  = ONE;
	return *this;
}

inline PSXVector & PSXVector::operator /= (const long s)
{
	x = fixedDiv(x,s);
	y = fixedDiv(y,s);
	z = fixedDiv(z,s);
	w  = ONE;
	return *this;
}

inline bool PSXVector::operator == (const PSXVector & rhs) const
{
	return (x == rhs.x) && (y == rhs.y) &&
	       (z == rhs.z) && (w == rhs.w);
}

inline PSXVector& PSXVector::operator = (const PSXVector& rhs)
{
    this->x = rhs.x;
    this->y = rhs.y;
    this->z = rhs.z;
    this->w = rhs.w;
    return *this;
}

//#undef ONE
#undef fixedMul
#undef fixedDiv

#endif // PSXMATH_VECTOR_HPP

