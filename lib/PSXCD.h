#ifndef PSX_CD_H_INCLUDED
#define PSX_CD_H_INCLUDED

// Assumes inBuf is the same size as the file to load
// fileName must be in the format "\\THE\\FILE\\PATH.EXT;1"
// returns true on success, false on failure
bool ReadFile( char* fileName, char* inBuf );

#endif // PSX_CD_H_INCLUDED

