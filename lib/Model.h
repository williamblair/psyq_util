/*
 * Model.h
 */

#ifndef MODEL_H_INCLUDED
#define MODEL_H_INCLUDED

#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "Texture.h"


typedef struct INDEX {
    int v0;
    int v1;
    int v2;
    int v3;
} INDEX;

/*
===============================================================================

Model

    Wraps mesh data and transformations for a 3d object
===============================================================================
*/
class Model
{
public:

    Model();
    virtual ~Model();

    void Rotate( int x, int y, int z );
    void Translate( int x, int y, int z );
    void Scale( int s );

    MATRIX modelMat;
    SVECTOR rotation;
    VECTOR position;    
    VECTOR scale;

    SVECTOR* vertices;
    SVECTOR* normals;
    INDEX* indices;
    u_short* texCoords;
    Texture* texture;

    size_t numVertices;
    size_t numNormals;
    size_t numIndices;

    inline void UpdateModelMatrix()
    {
        //ScaleMatrix( &modelMat, &modelMat);
        memset(&modelMat, 0, sizeof(modelMat));
        modelMat.m[0][0] = modelMat.m[1][1] = modelMat.m[2][2] = scale.vx;
        RotMatrix(   &rotation,    &modelMat );
        TransMatrix( &modelMat, &position );
    }
};

#endif // MODEL_H_INCLUDED

