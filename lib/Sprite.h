/*
 * Sprite.h
 */

#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>

#include "Texture.h"

/*
===============================================================================
Sprite

    Wraps a texture and a simple polygon to draw the texture.
===============================================================================
*/

class Sprite
{
public:

    friend class Renderer;

    /*
    ===================
    Constructor/Deconstructor
    ===================
    */
	Sprite();
    ~Sprite();

    /*
    ===================
    Parameters sent to the texture to load,
    See texture class for details
    ===================
    */
    void LoadTexture(
        u_long* texdata,  // pointer to texture data
        u_long* clutdata, // pointer to CLUT data (NULL if none)
        TEXTURE_BPP bpp,  // 0 - 16bit, 1 - 8bit, 2 - 4 bit
        int x,        // where in VRAM to load the texture
        int y,
        int w,        // size of the texture
        int h,
        int clutX,    // where to upload the clut data (if given)
        int clutY
    );

    /*
    ===================
    Instantly draws the primitive as opposed to adding it to the system
    order table, so sprites drawn AFTER this one will be drawn on top
    ===================
    */
	void Draw(void);

	/*
	===================
	Adds x and y to the sprite current position
	===================
	*/
	void Translate( int x, int y );

    /*
    ===================
    Setters
    ===================
    */
    // setXYWH already taken by psyq
    void SetPosAndSize( int x, int y, int w, int h );

	void SetX(int x);
	void SetY(int y);

	// set the ACTUAL width and height of the sprite, not its
	// texture size
	void SetWidth(int w);
	void SetHeight(int h);

	// set the clip location within the texture
	void SetU(int u);
	void SetV(int v);

    /*
    ===================
    Getters
    ===================
    */
    u_short GetClutId();
    u_short GetTextureId();

    int GetX();
    int GetY();


    /*
    ===================
    Increment the animator frame count and move to the
    Next sprite clip if necessary
    ===================
    */
	void Animate();

    /*
    ==================
    Use an existing texture instead of LoadTexture()
    ==================
    */
    void SetTexture( Texture& texture );


    /*
    ===================
    Upload a different clut (assumes load() has already been called with clut)
    ===================
    */
    void LoadClut( u_long* clut );

private:
	POLY_FT4 poly; // texture mapped polygon
	SVECTOR  pos;  // position/clip within texture

	// keep track of the x and y coordinates used by EACH
	// sprite instance, so we can figure out where to upload
	// textures in VRAM without overlapping each other
	static int vram_texture_x;
	static int vram_texture_y;

    // keep track of the x and y coordinates of cluts used
    // by EACH sprite instance, so we can figure out where
    // to upload cluts without overlapping each other
    static int vram_clut_x;
    static int vram_clut_y;

	u_short tex_id;  // texture id
	u_short clut_id; // clut id

	// size of the texture used by the sprite
	int tex_width, tex_height;

	// size of the sprite to be drawn
	int width, height;
	int clip_x,clip_y;  // offset/clip within texture

    // copy of where the clut was loaded in vram
    int clut_x, clut_y;

	// how many frames until the next sprite clip is used
	// and how many frames its been since the last clip changed
	int anim_speed;
	int anim_count;

    // Uploads the texture data and gets their GPU texture/clut IDs
    Texture texture;
};

#endif // SPRITE_H_INCLUDED

