#include <cstdio>
#include <fstream>

#include "models/player_model.h"

int main(int argc, char* argv[])
{
    std::ofstream md2file("models/pknight.md2", std::ios::binary);
    if ( !md2file.is_open() )
    {
        printf("Failed to open md2 out\n");
        return 1;
    }

    md2file.write( (const char*)player_model, size_player_model );
    md2file.close();

    return 0;
}
