#include <cstdio>
#include <cstdlib>
#include <cstdint>

/* Max texture width of a sprite */
#define MAX_SPR_SIZE 256
#define S_HEIGHT 256 

typedef struct {

    uint16_t header; // the header field
    uint32_t filesize; // size of the file in bytes

    uint32_t reserved; // 4 bytes of reserved data (depending on the image that creates it)

    uint32_t pix_offset; // 4 bytes  offset (i.e. starting address) of where the pixmap can be found

} BMP_header;

static bool enable_trans = true;

/*
 * Load BMP (assumes is 16bit)
 */
static void load_bmp(FILE *src, uint16_t *buffer, int *width, int *height)
{
    int32_t dib_header_size; // size of the bitmapv5 header
    int i,j;
    uint8_t r,g,b;
    uint16_t pixel_buffer[(MAX_SPR_SIZE+1)*S_HEIGHT]; // holds the pixels from the bmp, before we process them
    BMP_header header;

    uint32_t image_width, image_height;
    uint16_t bits_per_pixel = 16; // assuming this function is only called when the bmp is 16bpp
    uint32_t bytes_read;
    uint32_t padded_width; // the image width aligned/padded to the nearest 4 bytes

    /* Read in the BMP header */
    fread(&header.header, 2, 1, src); // Read in the header
    fread(&header.filesize, 4, 1, src); // Read in the file size
    fread(&header.reserved, 4, 1, src); // Read in reserved
    fread(&header.pix_offset, 4, 1, src); // Read in the pixel offset

    /* Read in the image size */
    fread(&dib_header_size, 4, 1, src);
    fread(&image_width, 4, 1, src);
    fread(&image_height, 4, 1, src);

    /* Store the image size back from the function */
    *width = image_width;
    *height = image_height;

    //printf("Image width,height: %d,%d\n", image_width, image_height);

    /* Go straight to the pixels */
    fseek(src, header.pix_offset, SEEK_SET);

    /* when reading, we might need to adjust width with 4byte aligned padding
     * e.g. the rowsize equation from here:
     *   https://en.wikipedia.org/wiki/BMP_file_format
     * align to 2 instead of 4 since uint16 = 2 bytes each
     */
    padded_width = image_width+(image_width%2);
    //printf("Padded width: %d\n", padded_width);

    if (bits_per_pixel != 16) {
        printf("!!!!!UNIMPLEMENTED BITS PER PIXEL!!!!!\n");
        printf("  Bits Per Pixel: %d\n", bits_per_pixel);
        return;
    }

    /* read in the pixel data */
    bytes_read = fread(pixel_buffer, 2, padded_width*image_height, src);
    //printf("Read %d bytes\n", bytes_read);

    /* now we need to flip Y, and swap the R and B components */
    for (i = 0; i < image_width; ++i) {
        for (j = 0; j < image_height; ++j) {

            /* get the pixel at the flipped y coordinate */
            uint16_t curpix = pixel_buffer[(image_height-1-j)*padded_width+i];

            b = curpix & 31;
            g = (curpix>>5)&31;
            r = (curpix>>10)&31;

            if (!enable_trans && b == 0 && g == 0 && r == 0)
            {
                // give a slight color so it's not transparent
                b = 1;
            }
    
            /* set the new color (switch b and r)
             * also y is flipped compared to curpix
             */
            buffer[j*image_width+i] = (b<<10) | (g<<5) | r;
        }
    }
}

inline void print_usage(int argc, char *argv[])
{
    printf("%s: convert a 16 bit BMP to a C array in PSX VRAM format\n", argv[0]);
    printf("Usage: %s <bmpfile> <array_name> <enabletrans (1 or 0)>\n", argv[0]);
}


int main(int argc, char *argv[])
{
    uint16_t *pixel_buffer = NULL;
    size_t  pixbuf_size;
    FILE    *bmp_file = NULL;
    int image_width;
    int image_height;

    // verify args
    if (argc != 4) {
        print_usage(argc, argv);
        return 0;
    }

    int trans_arg = atoi(argv[3]);
    enable_trans = (bool)trans_arg;

    // open the BMP file
    bmp_file = fopen(argv[1], "rb");
    if (!bmp_file) {
        printf("Failed to open file: %s\n", argv[1]);
        return -1;
    }

    // allocate memory for the array buffer
    fseek( bmp_file, 0, SEEK_END );
    pixbuf_size = ftell( bmp_file );
    fseek( bmp_file, 0, SEEK_SET );

    // divide size by 2 since using 16bits (2bytes) per array entry
    pixel_buffer = new uint16_t[pixbuf_size>>1];

    // read in the pixel data
    load_bmp( bmp_file, pixel_buffer, &image_width, &image_height );

    // TODO - support multiple BPP besides 16bit
    #define BYTES_PER_PIXEL 2

    // print out the pixel data as an array
    printf("u_char %s[%d] = {\n", argv[2], image_width*image_height*BYTES_PER_PIXEL);

    // TODO - add multiple BPP support (Besides sixteen bit)
    for ( size_t i = 0; i < image_width*image_height*BYTES_PER_PIXEL-1; ++i )
    {
        printf("0x%02X, ", ((uint8_t*)&pixel_buffer[0])[i]);
        if ((i+1) % 16 == 0) {
            printf("\n");
        }
    }
    printf("0x%02X\n", ((uint8_t*)&pixel_buffer[0])[image_width*image_height*BYTES_PER_PIXEL-1]);

    printf("};\n");

    // free memory
    fclose(bmp_file);
    delete[] pixel_buffer;

    return 0;
}

