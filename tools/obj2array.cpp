#include <iostream>
#include <fstream>
#include <string>

// OBJ_Loader - .obj Loader
#include "OBJ_Loader.h"

#define ONE 4096
static inline unsigned short float2psx(const float in)
{
    return (unsigned short)(float(ONE)*in);
}

// Main function
int main(int argc, char* argv[])
{
    if (argc != 3) {
        std::cout << "Usage: " << argv[0] << " <objfile> <outarrayname>" << std::endl;
        return 1;
    }

    // Initialize Loader
    objl::Loader Loader;

    // Load .obj File
    bool loadout = Loader.LoadFile(argv[1]);

    // Check to see if it loaded

    // If so continue
    if (loadout)
    {
        // Create/Open e1Out.txt
        std::ofstream file(std::string(argv[2]) + ".h");

        // Go through each loaded mesh and out its contents
        for (int i = 0; i < Loader.LoadedMeshes.size(); i++)
        {
            // Copy one of the loaded meshes to be our current mesh
            objl::Mesh curMesh = Loader.LoadedMeshes[i];

            std::cout << "Mesh " << i << ": " << curMesh.MeshName << "\n";

            // Go through each vertex and print its number,
            //  position, normal, and texture coordinate
            
            // Positions
            file << "SVECTOR " << argv[2] << "Vertices[] = {\n";
            for (int j = 0; j < curMesh.Vertices.size(); j++)
            {
                file << "{ "
                    << float2psx(curMesh.Vertices[j].Position.X) << ", "
                    << float2psx(curMesh.Vertices[j].Position.Y) << ", "
                    << float2psx(curMesh.Vertices[j].Position.Z) << ", 0"
                    << "},\n";
            }
            file << "};\n"
                << "size_t " << argv[2] << "NumVertices = " << curMesh.Vertices.size() << ";\n";

            // Normals
            file << "SVECTOR " << argv[2] << "Normals[] = {\n";
            for (int j = 0; j < curMesh.Vertices.size(); j++)
            {
                file << "{ "
                    << float2psx(curMesh.Vertices[j].Normal.X) << ", "
                    << float2psx(curMesh.Vertices[j].Normal.Y) << ", "
                    << float2psx(curMesh.Vertices[j].Normal.Z) << ", 0"
                    << "},\n";
            }
            file << "};\n"
                << "size_t " << argv[2] << "NumNormals = " << curMesh.Vertices.size() << ";\n";

            // Texture coords
            file << "u_short " << argv[2] << "TexCoords[] = {\n";
            for (int j = 0; j < curMesh.Vertices.size(); j++)
            {
                float u = curMesh.Vertices[j].TextureCoordinate.X;
                float v = curMesh.Vertices[j].TextureCoordinate.Y;
                u = fmodf(u,1.0f);
                v = fmodf(v,1.0f);
                if (u < 0.0f) {
                    u += 1.0f;
                }
                if (v < 0.0f) {
                    v += 1.0f;
                }
                file
                    << (unsigned short)(u*127.0f) << ", "
                    << (unsigned short)((1.0f-v)*127.0f) << ","
                    << "\n";
            }
            file << "};\n"
                << "size_t " << argv[2] << "NumTexCoords = " << curMesh.Vertices.size()*2 << ";\n";

            // Go through every 3rd index and print the
            //    triangle that these indices represent
            file << "INDEX " << argv[2] << "Indices[] = {\n";
            for (int j = 0; j < curMesh.Indices.size(); j += 3)
            {
                file << "{ " <<  curMesh.Indices[j + 2] << ", " << curMesh.Indices[j + 1] << ", " << curMesh.Indices[j + 0] << ", 0 },\n";
            }
            file << "};\n"
                << "size_t " << argv[2] << "NumIndices = " << curMesh.Indices.size()/3 << ";\n";

            // Print Material
            //file << "Material: " << curMesh.MeshMaterial.name << "\n";
            //file << "Ambient Color: " << curMesh.MeshMaterial.Ka.X << ", " << curMesh.MeshMaterial.Ka.Y << ", " << curMesh.MeshMaterial.Ka.Z << "\n";
            //file << "Diffuse Color: " << curMesh.MeshMaterial.Kd.X << ", " << curMesh.MeshMaterial.Kd.Y << ", " << curMesh.MeshMaterial.Kd.Z << "\n";
            //file << "Specular Color: " << curMesh.MeshMaterial.Ks.X << ", " << curMesh.MeshMaterial.Ks.Y << ", " << curMesh.MeshMaterial.Ks.Z << "\n";
            //file << "Specular Exponent: " << curMesh.MeshMaterial.Ns << "\n";
            //file << "Optical Density: " << curMesh.MeshMaterial.Ni << "\n";
            //file << "Dissolve: " << curMesh.MeshMaterial.d << "\n";
            //file << "Illumination: " << curMesh.MeshMaterial.illum << "\n";
            //file << "Ambient Texture Map: " << curMesh.MeshMaterial.map_Ka << "\n";
            //file << "Diffuse Texture Map: " << curMesh.MeshMaterial.map_Kd << "\n";
            //file << "Specular Texture Map: " << curMesh.MeshMaterial.map_Ks << "\n";
            //file << "Alpha Texture Map: " << curMesh.MeshMaterial.map_d << "\n";
            //file << "Bump Map: " << curMesh.MeshMaterial.map_bump << "\n";

            // Leave a space to separate from the next mesh
            file << "\n";
        }

        // Close File
        file.close();
    }
    // If not output an error
    else
    {
        // Output Error
        std::cout << "Failed to Load File. May have failed to find it or it was not an .obj file.\n";
    }

    // Exit the program
    return 0;
}

