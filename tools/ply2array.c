#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

typedef struct Vertex {
    float x;
    float y;
    float z;
} Vertex;

typedef struct Index {
    unsigned int v0;
    unsigned int v1;
    unsigned int v2;
} Index;

typedef struct Normal {
    float v0;
    float v1;
    float v2;
} Normal;


int loadPLY( const char *filename , Vertex **vertices, Index **indices, Normal **normals, int *retNumVertices, int *retNumIndices )
{
    *vertices = NULL;
    *indices  = NULL;
	
	bool has_normals = false;

    char currentLine[200];
    int lineLen = 200;

    char *token = NULL; // pointer used in strtok

    int retStatus = -1; // default to failure, set to 0 on success

    // open the file
    FILE *fp = fopen( filename, "r" );
    if ( fp == NULL ) {
        fclose( fp );
    }

    // read the header
    int numVertices = -1;
    int numIndices = -1;
    int foundEndHeader = 0;
    while ( fgets (currentLine, lineLen, fp ) != NULL && !foundEndHeader ) 
    {
        //printf( "Line: %s\n", currentLine );

        // get each word in the line
        token = strtok( currentLine, " " );
        while ( token != NULL ) {
            //printf("    Word: %s\n", token );

            // exit when found end_header
            if ( strcmp( token, "end_header" ) == 0 || 
                 strcmp( token, "end_header\n") == 0 ) {
                //printf("Found end header!\n");
                foundEndHeader = 1;
                break;
            }
			// assuming using blender's syntax for normals (nx, ny, nz)
			if ( strcmp( token, "nx\n" ) == 0 ||
                 strcmp( token, "nx" ) == 0 ) {
				has_normals = true;
			}

            // read number of vertices
            if ( strcmp(token, "element") == 0 )
            {
                // read the next word (assuming vertex or something)
                token = strtok( NULL, " " );
                //printf("Element Type: %s\n", token);

                // get the number of vertices and indices (assuming vertices comes first)
                token = strtok( NULL, " " );
                if ( numVertices == -1 ) {
                    sscanf( token, "%d", &numVertices );
                    //printf( "Num Vertices: %d\n", numVertices );
                    *retNumVertices = numVertices;
                } 
                else {
                    sscanf( token, "%d", &numIndices );
                    //printf( "Num Indices: %d\n", numIndices );
                    *retNumIndices = numIndices;
                }
            }

            token = strtok( NULL, " " );
        }
    }

    // allocate memory for vertices/indices
    *vertices = malloc( sizeof(Vertex) * numVertices );
    if ( *vertices == NULL ) {
        //printf("Failed to alloc vertices data!\n");
        goto done;
    }
	if (has_normals) {
		*normals = malloc( sizeof(Normal) * numVertices );
		if ( *normals == NULL ) {
			goto done;
		}
	}
    *indices =  malloc( sizeof(Index) * numIndices );
    if ( *indices == NULL ) {
        //printf("Failed to alloc indices data!\n");
        goto done;
    }

    // read vertices
    for (int i = 0; i < numVertices; ++i)
    {
        // get da vertices
        token = strtok( currentLine, " " );   // X
        sscanf( token, "%f", &(*vertices)[i].x);
        token = strtok( NULL, " " );          // Y
        sscanf( token, "%f", &(*vertices)[i].y);
        token = strtok( NULL, " " );          // Z
        sscanf( token, "%f", &(*vertices)[i].z);

		if ( has_normals ) {
			// get the normals (ASSUMES WE HAVE NORMALS!)
			token = strtok( NULL, " " );   // X
			sscanf( token, "%f", &(*normals)[i].v0);
			token = strtok( NULL, " " );          // Y
			sscanf( token, "%f", &(*normals)[i].v1);
			token = strtok( NULL, " " );          // Z
			sscanf( token, "%f", &(*normals)[i].v2);
        }

        // read the next line and error check
        if ( fgets (currentLine, lineLen, fp ) == NULL ) 
        {
            printf("Failed reading all vertices, i: %d\n", i);
            goto done;
        }

    }

    // read indices
    int dummyholder = 0; // holds the first number on each line
    for ( int i = 0; i < numIndices; ++i )
    {

        // get the indices
        // first number tells how many are on that line; we assume 3
        token = strtok( currentLine, " " );
        sscanf( token, "%d", &dummyholder );
        token = strtok( NULL, " " );
        sscanf( token, "%d", &(*indices)[i].v2 );
        token = strtok( NULL, " " );
        sscanf( token, "%d", &(*indices)[i].v1 );
        token = strtok( NULL, " " );
        sscanf( token, "%d", &(*indices)[i].v0 );

        // read the next line and error check
        // check for second to last index since this SHOULD fail on the last try
        if ( fgets (currentLine, lineLen, fp ) == NULL && i != numIndices - 1)
        {
            printf("Failed reading all indices!\n");
            goto done;
        }

    }

    retStatus = 0; // if we reached here all good

done:
    if ( fp != NULL ) {
        //printf("Closing fp!\n");
        fclose( fp );
    }
    #if 0
    if ( vertices != NULL ) {
        free( vertices );
    }
    if ( indices != NULL ) {
        free( indices );
    }
    #endif
    return retStatus;
}

void calcluateNormals( Vertex **vertices, Index **indices, Normal **normals, int numVertices, int numIndices )
{
	size_t faceIndex;
	double normalX;
	double normalY;
	double normalZ;
	
	*normals = (Normal*)malloc(sizeof(Normal) * numIndices);
	if (*normals == NULL) {
		printf("Failed to allocate normals memory\n");
		return;
	}
	
#define X1	(*vertices)[(*indices)[faceIndex].v0].x
#define X2	(*vertices)[(*indices)[faceIndex].v1].x
#define X3	(*vertices)[(*indices)[faceIndex].v2].x
#define Y1	(*vertices)[(*indices)[faceIndex].v0].y
#define Y2	(*vertices)[(*indices)[faceIndex].v1].y
#define Y3	(*vertices)[(*indices)[faceIndex].v2].y
#define Z1	(*vertices)[(*indices)[faceIndex].v0].z
#define Z2	(*vertices)[(*indices)[faceIndex].v1].z
#define Z3	(*vertices)[(*indices)[faceIndex].v2].z
	
	// for each face
	for ( faceIndex = 0; faceIndex < numIndices; ++faceIndex )
	{
		// calculate its normal
		// https://math.stackexchange.com/questions/305642/how-to-find-surface-normal-of-a-triangle
		normalX = ((Y2 - Y1) * (Z3 - Z1)) - ((Y3 - Y1) * (Z2 - Z1));
		normalY = ((Z2 - Z1) * (X3 - X1)) - ((X2 - X1) * (Z3 - Z1));
		normalZ = ((X2 - X1) * (Y3 - Y1)) - ((X3 - X1) * (Y2 - Y1));
		
		float normalSum = normalX + normalY + normalZ;// + 0.000001F; // add so that bottom isn't zero
		
		// prevent divide by zero
		if (fabs(normalSum) < 0.000001F) {
			normalSum = 0.00001F;
		}
		
		(*normals)[faceIndex].v0 = normalX / normalSum;
		(*normals)[faceIndex].v1 = normalY / normalSum;
		(*normals)[faceIndex].v2 = normalZ / normalSum;
		
		//printf("Normal x,y,z: %f,%f,%f\n", (*normals)[faceIndex].v0, (*normals)[faceIndex].v1, (*normals)[faceIndex].v2);
	}
	
	return;
}

void printUsage(int argc, char *argv[])
{
    printf("Usage: %s <plyfile> <arrayname>\n", argv[0]);
}

// PSX coordinates scale
//#define ONE 4096
#define ONE 1024

int main(int argc, char *argv[])
{
    Vertex * vertices = NULL;
    int numVertices = 0;
    Index  * indices  = NULL;
    int numIndices = 0;

    Normal *normals = NULL;

    int numDiv = 10; // how much to skip when sampling?...
    
    if ( argc < 3 ) {
        printUsage( argc, argv );
        return 0;
    }

    if ( loadPLY( argv[1], &vertices, &indices, &normals, &numVertices, &numIndices) != 0 ) {
        printf("Failed load ply...\n");
    } 
#if 1
    // array time!
    printf("SVECTOR %s_vertices[%d] = {\n", argv[2], numVertices);
    for (int i = 0; i < numVertices; ++i) {
        printf("  { %d, %d, %d, %d },\n", (short)(vertices[i].x*ONE), (short)(vertices[i].y*ONE), (short)(vertices[i].z*ONE), 0 );
    } printf("};\n\n");
#endif
	if ( normals == NULL ) {
		printf("No normals! calculating ourselves...\n");
		calcluateNormals(&vertices, &indices, &normals, numVertices, numIndices);
	}

    printf("SVECTOR %s_normals[%d] = {\n", argv[2], numIndices);
    for (int i = 0; i < numIndices; ++i) {
        printf("  { %d, %d, %d, %d },\n", -(short)(normals[i].v0*ONE), (short)(normals[i].v1*ONE), (short)(normals[i].v2*ONE), 0 );
    } printf("};\n\n");

#if 1
    printf("INDEX %s_indices[%d] = {\n", argv[2], numIndices);
    for (int i = 0; i < numIndices; ++i) {
        printf("  { %d, %d, %d, %d },\n", indices[i].v0, indices[i].v1, indices[i].v2, 0 );
    } printf("};\n");
#endif
    printf("int num_%s_vertices = %d;\n", argv[2], numVertices);
    printf("int num_%s_indices = %d;\n", argv[2], numIndices);
    printf("int num_%s_normals = %d;\n", argv[2], numVertices);

    if ( vertices != NULL ) {
        free( vertices );
    }
    if ( indices != NULL ) {
        free( indices );
    }
    if ( normals != NULL ) {
        free( normals );
    }

    // bj dumm
    //while(1);
    
    return 0;
}

