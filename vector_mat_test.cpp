/*
 * main.cpp
 */

#include <stdio.h>
#include <libmath.h>

#include <PSXRenderer.h>
#include <Model.h>
#include <Cube.h>
#include <Camera.h>
#include <Controller.h>
#include <Texture.h>
#include <Sprite.h>
#include <Light.h>
#include <Font.h>
#include <array.hpp>
#include <PSXVector.h>
#include <PSXMatrix.h>
#include <Md2Model.h>

#include "suzanne_model.h"
#include "terminal_font_array.h"

// for(ever) ;)
#define ever ;;

class FloorObj : public Model
{
public:

    FloorObj() {
        printf("FloorObj constructor!\n");

        this->vertices = this->floorVertices;
        this->normals = this->floorNormals;

        this->numVertices = 4;
        this->numNormals = 1;
    }

    virtual ~FloorObj() {}

private:

    // floor points
    static SVECTOR P0;
    static SVECTOR P1;
    static SVECTOR P2;
    static SVECTOR P3;

    // floor normals
    static SVECTOR N0;

    // vertices - pointers to coordinates
    static SVECTOR floorVertices[4];

    // normals - pointers to normals vectors
    static SVECTOR floorNormals[1];
};

SVECTOR FloorObj::P0 = {-ONE,0,-ONE,0};
SVECTOR FloorObj::P1 = {ONE,0,-ONE,0};
SVECTOR FloorObj::P3 = {-ONE,0,ONE,0};
SVECTOR FloorObj::P2 = {ONE,0,ONE,0};

SVECTOR FloorObj::N0 = {0,ONE,0,0};

SVECTOR FloorObj::floorVertices[4] =
{
    P0,P1,P2,P3
};

SVECTOR FloorObj::floorNormals[1] =
{
    N0
};

static void initSuzanne( Model& suzanne )
{
    suzanne.vertices = suzanne_test_vertices;
    suzanne.numVertices = num_suzanne_test_vertices;
    suzanne.indices = suzanne_test_indices;
    suzanne.numIndices = num_suzanne_test_indices;
    suzanne.normals = suzanne_test_normals;
    suzanne.numNormals = num_suzanne_test_normals;

    suzanne.Translate( 0, 0, 8000 );
    //suzanne.Translate( 0, 0, 0 );
}

// defined in PSXMatrix.h
//static inline long float2fixed(const float a)
//{
//    return (long)(a * 4096);
//}

static inline float fixed2float(const long a)
{
    return ((float)a) / 4096.0f;
}
#define PI 3.1415926535f
#define PI2 (PI*2.0f)
static inline long radians2fixed(const float radians)
{
    return (long)(4096.0f*(radians/PI2));
}
#define fixedDiv(a,b) ((long)((((long long)(a))<<12) / ((long long)(b))))

static void testRotate()
{
    PSXMatrix rotMat;
    rotMat.makeRotationZ( radians2fixed(PI/2.0f) );
    PSXVector testVec( float2fixed(1.0f), 0, 0, ONE );
    testVec = rotMat * testVec;
    printf2("Rotated around Z: %f, %f, %f, %f\n", fixed2float(testVec.x),
                                                  fixed2float(testVec.y), 
                                                  fixed2float(testVec.z), 
                                                  fixed2float(testVec.w) );

    rotMat.makeRotationX( radians2fixed(PI/2.0f) );
    testVec = PSXVector( 0, 0, float2fixed(1.0f), ONE );
    testVec = rotMat * testVec;
    printf2("Rotated around X: %f, %f, %f, %f\n", fixed2float(testVec.x),
                                                  fixed2float(testVec.y), 
                                                  fixed2float(testVec.z), 
                                                  fixed2float(testVec.w) );

    rotMat.makeRotationY( radians2fixed(PI/2.0f) );
    testVec = PSXVector( float2fixed(1.0f), 0, 0, ONE );
    testVec = rotMat * testVec;
    printf2("Rotated around Y: %f, %f, %f, %f\n", fixed2float(testVec.x),
                                                  fixed2float(testVec.y), 
                                                  fixed2float(testVec.z), 
                                                  fixed2float(testVec.w) );
}

int main(void)
{
    PSXMatrix testMat;
    PSXVector vecA;
    PSXVector vecB;
    PSXVector vecC;
#if 0
    Model suzanne;
    Cube cube;
    FloorObj floorObj;
#endif
    PSXRenderer renderer;
#if 0
    Camera camera;
    Controller controller;
    Sprite testSprite;
    Font font;
    Light light;
    Light blueLight;
#endif

    // Order here matters for initialization
    renderer.Init();
#if 0
    Controller::Init();
    controller.InitController(0);

    initSuzanne( suzanne );

    cube.Translate( -400, 0, 500 );
    floorObj.Translate( -400, -ONE, 500 );

    light.SetColor( ONE, 0, 0 );
    blueLight.SetColor( 0, 0, ONE );
    blueLight.Translate( -400, 0, 500 );

    renderer.SetCamera( camera );
    renderer.AddLight( light );
    renderer.AddLight( blueLight );

    testSprite.SetTexture( Texture::DefaultTexture );
    testSprite.SetPosAndSize( 20, 20, 30, 30 );

    font.LoadTexture( (u_long*)terminal_font_array,
                     NULL,
                     TEXTURE_16BIT,
                     384, 0,
                     74, 50,
                     0, 0 );
    font.SetWidthAndHeight( 9.25, 13 );

    printf("Light Color matrix:\n");
    renderer.printLightColMat();

    printf("Light Position matrix:\n");
    renderer.printLightColMat();

    for (ever)
    {
        controller.Read();

        suzanne.Rotate( 20, 0, 40 );
        cube.Rotate( 4, 0, 2 );

        if (controller.IsHeld(PadLeft))
        {
            camera.Translate( -1.0f, 0.0f, 0.0f );
        }
        if (controller.IsHeld(PadRight))
        {
            camera.Translate( 1.0f, 0.0f, 0.0f );
        }
        if (controller.IsHeld(PadUp))
        {
            camera.Translate( 0.0f, 0.0f, -1.0f );
        }
        if (controller.IsHeld(PadDown))
        {
            camera.Translate( 0.0f, 0.0f, 1.0f );
        }

        if (controller.IsHeld(PadSquare))
        {
            camera.Rotate( 0.0f, -1.0f, 0.0f );
        }
        if (controller.IsHeld(PadCircle))
        {
            camera.Rotate( 0.0f, 1.0f, 0.0f );
        }
        if (controller.IsHeld(PadTriangle))
        {
            camera.Rotate( 1.0f, 0.0f, 0.0f );
        }
        if (controller.IsHeld(PadCross))
        {
            camera.Rotate( -1.0f, 0.0f, 0.0f );
        }

        camera.UpdateMat();
        renderer.DrawModel( suzanne );
        renderer.DrawModel( cube );
        renderer.DrawModel( floorObj );
        renderer.DrawSprite( testSprite );
        renderer.DrawString( font, 10, 50, "hello world" );
        renderer.Update();
    }
#endif

    printf2("Float to fixed 1.0f: %f, %d\n", 1.0f, float2fixed(1.0f));
    printf2("Fixed to float 4096: %d, %f\n", 4096, fixed2float(4096));

    testMat.makeScaling(float2fixed(2.0f),float2fixed(2.0f),float2fixed(2.0f));

    vecA = PSXVector(float2fixed(1.0f), float2fixed(2.0f), float2fixed(4.0f), float2fixed(1.0f));
    vecB = PSXVector(float2fixed(3.0f), float2fixed(2.0f), float2fixed(4.0f), float2fixed(1.0f));
    vecC = PSXVector(float2fixed(3.0f), float2fixed(2.0f), float2fixed(4.0f), float2fixed(1.0f));

    vecA *= float2fixed(2.0f);
    vecB /= float2fixed(2.0f);
    vecC.normalizeSelf();

    printf2("1.0/2.0 : %f\n", fixed2float(fixedDiv(float2fixed(1.0f), float2fixed(2.0f))));
    printf2("2.0/4.0 : %f\n", fixed2float(fixedDiv(float2fixed(2.0f), float2fixed(4.0f))));
    printf2("8.0/16.0: %f\n", fixed2float(fixedDiv(float2fixed(8.0f), float2fixed(16.0f))));

    printf2("a*b: %f, %f, %f, %f\n", fixed2float(vecA.x),
                                     fixed2float(vecA.y), 
                                     fixed2float(vecA.z), 
                                     fixed2float(vecA.w) );
    vecA = testMat * vecA;
    printf2("scaled a*b 2: %f, %f, %f, %f\n", fixed2float(vecA.x),
                                              fixed2float(vecA.y), 
                                              fixed2float(vecA.z), 
                                              fixed2float(vecA.w) );
    testMat.makeScaling(float2fixed(0.25f),float2fixed(0.25f),float2fixed(0.25f));
    vecA = testMat * vecA;
    printf2("scaled a*b 0.25: %f, %f, %f, %f\n", fixed2float(vecA.x),
                                                 fixed2float(vecA.y), 
                                                 fixed2float(vecA.z), 
                                                 fixed2float(vecA.w) );

    testMat.makeTranslation(float2fixed(1.0f), float2fixed(2.0f), float2fixed(-1.0f));
    vecA = testMat * vecA;
    printf2("scaled a*b 0.25 translated: %f, %f, %f, %f\n", fixed2float(vecA.x),
                                                            fixed2float(vecA.y), 
                                                            fixed2float(vecA.z), 
                                                            fixed2float(vecA.w) );

    printf2("a/b: %f, %f, %f, %f\n", fixed2float(vecB.x),
                                     fixed2float(vecB.y), 
                                     fixed2float(vecB.z), 
                                     fixed2float(vecB.w) );
    printf2("c normal: %f, %f, %f, %f\n", fixed2float(vecC.x),
                                   fixed2float(vecC.y), 
                                   fixed2float(vecC.z), 
                                   fixed2float(vecC.w) );

    testRotate();

    while (1);

    return 0;
}


