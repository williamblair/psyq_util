/*
 * cdread.cpp
 */

#include <stdio.h>
#include <libmath.h>

#include <PSXRenderer.h>
#include <Model.h>
#include <Cube.h>
#include <Camera.h>
#include <Controller.h>
#include <Texture.h>
#include <Sprite.h>
#include <Light.h>
#include <Font.h>
#include <array.hpp>
#include <PSXVector.h>
#include <PSXMatrix.h>
#include <Md2Model.h>

#include <libcd.h>
#include <libapi.h>

#include "terminal_font_array.h"

// for(ever) ;)
#define ever ;;

static inline float fixed2float(const long a)
{
    return ((float)a) / 4096.0f;
}
#define PI 3.1415926535f
#define PI2 (PI*2.0f)
static inline long radians2fixed(const float radians)
{
    return (long)(4096.0f*(radians/PI2));
}
#define fixedDiv(a,b) ((long)((((long long)(a))<<12) / ((long long)(b))))

bool ReadFile( char* fileName, char* inBuf )
{
    char* buf = nullptr; // store the read file
    CdlFILE fileStruct;
    int i;
    int cnt;
    memset( &fileStruct, 0, sizeof(fileStruct) );

    // check disk status
    u_char result[8];
    CdControl( CdlNop, 0, result );
    if ( result[0] & CdlStatShellOpen )
    {
        if ( CdDiskReady(1) != CdlComplete )
        {
            printf("Shell open waiting\n");
            while ( CdDiskReady(0) != CdlComplete );
        }
    }

    // name MUST be uppsercase in the form "\\THE\\FILE\\PATH.EXT;1"
    unsigned long bufSize = 0;
    if ( CdSearchFile( &fileStruct, fileName ) == 0 )
    {
        printf( "CD search file failed (pos: 0x%X)\n", fileStruct.pos );
        return false;
    }
    else
    {
        printf( "Found file: pos,size,name: 0x%08X, %lu, %s\n",
                *((u_int*)&fileStruct.pos),
                fileStruct.size,
                fileStruct.name );
        const unsigned long sectorSize = 2048;
        unsigned long bufSize = fileStruct.size + (sectorSize - (fileStruct.size % sectorSize));
        buf = new char[bufSize];
    }


    // start the read operation
    // num bytes (size) must be a multiple of 2048
    //      reads entire file if num bytes is 0
    CdReadFile( fileName, (u_long*)buf, 0 ); 

    // wait for read operation to finish
    while (( cnt = CdReadSync(1, 0) ) > 0 );
    if ( cnt == -1 ) {
        printf( "CdReadSync error\n" );
        delete[] buf;
        return false;
    }
    memcpy( inBuf, buf, fileStruct.size );
    delete[] buf;
    buf = nullptr;

    return true;
}

int main(void)
{
    PSXRenderer renderer;
    Font font;
    Camera camera;
    char textBuf[512] = {0};

    // Order here matters for initialization
    renderer.Init();

    font.LoadTexture( (u_long*)terminal_font_array,
                     NULL,
                     TEXTURE_16BIT,
                     384, 0,
                     74, 50,
                     0, 0 );
    font.SetWidthAndHeight( 9.25, 13 );

    renderer.SetCamera( camera );

    if ( ReadFile( "\\TEST.TXT;1", textBuf ) )
    {
        printf( "Read test.txt: %s\n", textBuf );
    }
    
    for (ever)
    {
        renderer.DrawString( font, 10, 50, textBuf );
        renderer.Update();
    }

    return 0;
}


