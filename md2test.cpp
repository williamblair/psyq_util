/*
 * main.cpp
 */

#include <stdio.h>
#include <string.h>
#include <libmath.h> // for printf2

#include <PSXRenderer.h>
#include <Model.h>
#include <Cube.h>
#include <Camera.h>
#include <Controller.h>
#include <Texture.h>
#include <Sprite.h>
#include <Light.h>
#include <Font.h>
#include <array.hpp>
#include <PSXVector.h>
#include <PSXMatrix.h>
#include <Md2Model.h>
#include <PSXCD.h>

//#include "suzanne_model.h"
//#include "models/player_model.h"
#include "models/player_texture.h"
#include "terminal_font_array.h"

// for(ever) ;)
#define ever ;;

// from https://github.com/sk-io/psx-engine/blob/main/src/main.c
volatile int fps = 0;
volatile int fps_counter = 0;
volatile int fps_measure = 0;
static int vsync_time = 0;
static char fps_str[50] = {0};
static volatile float timeFloatCounter = 0.0f;

void vsyncCallback()
{
    fps_counter++;
    if (fps_counter >= 60)
    {
        fps = fps_measure;
        fps_measure = 0;
        fps_counter = 0;
    }
    timeFloatCounter += 1.0f/60.0f; // TODO  - not 100% sure this is accurate
}

class FloorObj : public Model
{
public:

    FloorObj() {
        printf("FloorObj constructor!\n");

        this->vertices = this->floorVertices;
        this->normals = this->floorNormals;

        this->numVertices = 4;
        this->numNormals = 1;
    }

    virtual ~FloorObj() {}

private:

    // floor points
    static SVECTOR P0;
    static SVECTOR P1;
    static SVECTOR P2;
    static SVECTOR P3;

    // floor normals
    static SVECTOR N0;

    // vertices - pointers to coordinates
    static SVECTOR floorVertices[4];

    // normals - pointers to normals vectors
    static SVECTOR floorNormals[1];
};

SVECTOR FloorObj::P0 = {-ONE,0,-ONE,0};
SVECTOR FloorObj::P1 = {ONE,0,-ONE,0};
SVECTOR FloorObj::P3 = {-ONE,0,ONE,0};
SVECTOR FloorObj::P2 = {ONE,0,ONE,0};

SVECTOR FloorObj::N0 = {0,ONE,0,0};

SVECTOR FloorObj::floorVertices[4] =
{
    P0,P1,P2,P3
};

SVECTOR FloorObj::floorNormals[1] =
{
    N0
};

// defined in PSXMatrix.h
//static inline long float2fixed(const float a)
//{
//    return (long)(a * 4096);
//}

static inline float fixed2float(const long a)
{
    return ((float)a) / 4096.0f;
}
#define PI 3.1415926535f
#define PI2 (PI*2.0f)
static inline long radians2fixed(const float radians)
{
    return (long)(4096.0f*(radians/PI2));
}
#define fixedDiv(a,b) ((long)((((long long)(a))<<12) / ((long long)(b))))

static void initMd2ModelMat( PSXMatrix& mat )
{
    static SVECTOR rotation;
    static VECTOR position;
    setVector(&rotation, 0,0,0);
    setVector(&position, 0, 0, 500);

    RotMatrix( &rotation, (MATRIX*)&mat );
    TransMatrix( (MATRIX*)&mat, &position );
}

static u_char* md2buf = nullptr;
static bool loadMd2Data( Md2Model& md2Model )
{
    md2buf = new u_char[303216]; // TODO - not hardcode size
    if ( !md2buf )
    {
        printf("Failed to alloc md2buf\n");
        return false;
    }

    if ( !ReadFile( "\\MODELS\\PKNIGHT.MD2;1", (char*)md2buf ) )
    {
        printf("Failed to read md2 file\n");
        return false;
    }

    if (!md2Model.InitFromMemory( md2buf,
                                  303216, // TODO - not hard code size
                                  0.025f )) // scale
    {
        printf("Failed to init model from memory\n");
        return false;
    }

    return true;
}

int main(void)
{
    Cube cube;
    FloorObj floorObj;
    PSXRenderer renderer;
    Camera camera;
    Controller controller;
    Sprite testSprite;
    Font font;
    Light light;
    Light blueLight;
    Md2Model md2Model;
    Texture md2Texture;
    PSXMatrix md2ModelMat;

    // Order here matters for initialization
    renderer.Init();
    Controller::Init();
    controller.InitController(0);

    VSyncCallback( vsyncCallback );

    cube.Translate( -400, 0, 500 );
    floorObj.Translate( -400, -ONE, 500 );

    light.SetColor( ONE, 0, 0 );
    blueLight.SetColor( 0, 0, ONE );
    blueLight.Translate( -400, 0, 500 );

    renderer.SetCamera( camera );
    renderer.AddLight( light );
    renderer.AddLight( blueLight );

    testSprite.SetTexture( Texture::DefaultTexture );
    testSprite.SetPosAndSize( 20, 20, 30, 30 );

    font.LoadTexture( (u_long*)terminal_font_array,
                     NULL,
                     TEXTURE_16BIT,
                     384, 0,
                     74, 80,
                     0, 0 );
    font.SetWidthAndHeight( 9.25, 13 );

    md2Texture.Load(
        (u_long*)player_texture_array,
        NULL,
        TEXTURE_16BIT,
        512, 0,         // vram location
        255, 255,       // width, height
        0, 0 );

    printf("Light Color matrix:\n");
    renderer.printLightColMat();

    printf("Light Position matrix:\n");
    renderer.printLightColMat();

    if ( !loadMd2Data( md2Model ) )
    {
        while(1);
    }
    md2ModelMat.makeIdentity();
    initMd2ModelMat( md2ModelMat );
    md2Model.SetAnim( MD2ANIM_RUN );

    for (ever)
    {
        controller.Read();

        cube.Rotate( 4, 0, 2 );

        if (controller.IsHeld(PadLeft))
        {
            camera.Translate( -1.0f, 0.0f, 0.0f );
        }
        if (controller.IsHeld(PadRight))
        {
            camera.Translate( 1.0f, 0.0f, 0.0f );
        }
        if (controller.IsHeld(PadUp))
        {
            camera.Translate( 0.0f, 0.0f, -1.0f );
        }
        if (controller.IsHeld(PadDown))
        {
            camera.Translate( 0.0f, 0.0f, 1.0f );
        }

        if (controller.IsHeld(PadSquare))
        {
            camera.Rotate( 0.0f, -1.0f, 0.0f );
        }
        if (controller.IsHeld(PadCircle))
        {
            camera.Rotate( 0.0f, 1.0f, 0.0f );
        }
        if (controller.IsHeld(PadTriangle))
        {
            camera.Rotate( 1.0f, 0.0f, 0.0f );
        }
        if (controller.IsHeld(PadCross))
        {
            camera.Rotate( -1.0f, 0.0f, 0.0f );
        }

        sprintf(fps_str, "fps %d", fps);

        camera.UpdateMat();
        renderer.DrawModel( cube );
        renderer.DrawModel( floorObj );
        md2Model.UpdateAnim( timeFloatCounter );
        renderer.DrawMd2Model( md2Model, md2ModelMat, md2Texture );
        renderer.DrawSprite( testSprite );
        renderer.DrawString( font, 10, 50, "hello world" );
        renderer.DrawString( font, 10, 60, fps_str );
        renderer.Update();

        fps_measure++;
    }

    free( md2buf );
    md2buf = nullptr;
    return 0;
}


