::set PATH=D:\psyq\bin
::set PSYQ_PATH=D:\psyq\bin

set prog=main
set address=$80100000
set cflags=-O3 -Ilib

::set libsources="lib/System.cpp lib/Model.cpp lib/Model_textured.cpp lib/Cube.cpp lib/Light.cpp lib/Pad.cpp Camera.cpp FPS_movable_object.cpp Texture.cpp Sprite.cpp Cube_textured.cpp Font.cpp"
set libsources=lib/*.cpp

::set sources=main.cpp
set sources=md2test.cpp
::set sources="third_pers_main.cpp"

ccpsx -Xo%address% %cflags% %libsources% %sources% -o%prog%.cpe,%porg%.sym,mem.map -llibpad

cpe2x %prog%.cpe
