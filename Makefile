TARGET = md2test
#TARGET = main
#TARGET = cdread

all:
	ccpsx -Xo$$80100000 -O3 -Dnullptr=NULL -Ilib lib/*.cpp $(TARGET).cpp -o$(TARGET).cpe,$(TARGET).sym,mem.map -llibpad -llibcd
	cpe2x $(TARGET).cpe

cd:
	rm -rf cd_root
	mkdir -p cd_root
	cp $(TARGET).exe cd_root
	echo "file read test" > cd_root/test.txt
	cp -r models cd_root
	systemcnf.exe $(TARGET).exe > cd_root/system.cnf
	mkisofs.exe -o $(TARGET).hsf -V MAIN -sysid PLAYSTATION cd_root
	mkpsxiso.exe $(TARGET).hsf $(TARGET).bin D:/Programming/psx/psx-sdk/share/licenses/infousa.dat

clean:
	rm -rf *.cpe *.exe *.sym mem.map cd_root *.bin *.cue *.hsf
